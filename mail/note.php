
<h3>У вас новая запись!</h3>


<p><b>Дата: </b><?= $note->textDate ?></p>
<p><b>Время: </b><?= $note->textTime ?></p>
<p><b>Сотрудник: </b><?= $worker->name ?></p>
<p><b>Услуга: </b><?= $note->service ?></p>
<br/>
<p><b>Имя: </b><?= $note->name ?></p>
<p><b>Телефон: </b><?= $note->phone ?></p>

<? if( !empty($note->comment) ): ?>
   
    <p><b>Комментарий</b></p>
    <p><?= $note->comment ?></p>
    
<? endif; ?>

<br/>

<p>Вы можете просмотреть и изменить запись в журнале по этой <a href="http://ionlion.ru/journal/?id=<?= $note->worker_id ?>&date=<?= $note->dateUnix ?>">ссылке</a></p>