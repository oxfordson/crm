<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

use yii\db\Query;

use app\models\Staff;
use app\models\Notes;
use app\models\Services;

class StatsController extends Controller
	{
	
	
        public function behaviors() {
            
            return [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ],
            ];
            
        }
			
		
		public function actionIndex()
			{	

				return $this->render('stats');
            
			}
    
    
		public function actionGet()
			{
            
                $endUnix           = ( !empty( $_POST['to'] ) ) ? $_POST['to'] :  Yii::$app->date->dateUnix('');
            
                $unix = $startUnix = ( !empty( $_POST['from'] ) ) ? $_POST['from'] : $endUnix - 86400 * 14;
            
            
                $chart = ( !empty( $_POST['chart'] ) ) ? $_POST['chart'] : 'total';
            
            
                $period = ( !empty( $_POST['p'] ) ) ? $_POST['p'] : 1;
            
                $pUnix = $period * 86400;
            
            
            
                $data = array();
            
                $tick = 0;
            
                $user = [ 'user_id' => Yii::$app->user->id ];
            
            
                $i = ceil ( ( ($endUnix - $unix) / $pUnix + 1 ) / 8 );
            
            
                while( $unix <= $endUnix ){
            
                    $textDate = Yii::$app->date->rusShortDate($unix);
                    
                    if($period == 1){
                    
                        $dates = ['dateUnix' => $unix ];
                            
                        $pText = Yii::$app->date->rusDate($unix);
                        
                    }else{
                        
                        
                        $pEnd = $unix + $pUnix;
                        
                        $dates = ["AND", "dateUnix >= $unix", "dateUnix <= $pEnd" ];
                        
                        $pStartDate = Yii::$app->date->rusShortDate($unix);
                        
                        $pEndDate = Yii::$app->date->rusShortDate($pEnd);
                        
                        $pText = "$pStartDate — $pEndDate";
                            
                        
                    }
                    
                    $notes = Notes::find()->where( $user )->andWhere( $dates );
                    
                    $tooltip = "$pText<br>Записей: ";
                    
                    
                    switch ($chart) {
                            
                        case "done":
                            
                            $value = $notes->andWhere( ["status" => 2 ] )->count(); 
                            break;
                            
                        case "admin":
                            
                            $value = $notes->andWhere( ["admin" => 1 ] )->count(); 
                            break;
                            
                        case "client":
                            
                            $value = $notes->andWhere( ["admin" => 0 ] )->count(); 
                            break;
                            
                        case "profit":
                            
                            $value = $notes->andWhere( ["status" => 1 ] )->sum('price');
                            
                            $tooltip = "$pText<br>Прибыль: ";
                            
                            break;
                            
                        default:
                            
                            $value = $notes->count();
                            
                    }
                    
                    
                    $tooltip .= intval($value);

                    
                    if( $tick == $i || $tick == 0){
                        
                        $text = $textDate;
                        
                        $tick = 1;
                        
                    }else{
                        
                        $text = '';
                        
                        $tick++;
                        
                    }
                    
                    
                    $data[] = [

                        'value' => $value,
                        'tick' => $text,
                        'tooltip' => $tooltip

                    ];
            
                    
                    $unix += $pUnix;
                    
                }
                         
                $dates = ["AND", "dateUnix >= $startUnix", "dateUnix <= $endUnix" ];
            
            
                $personel = $services = [];
            
            
                $staff = Staff::find()->where($user)->all();
            
                foreach($staff as $worker){
                    
                    $worker_id = ['worker_id' => $worker->id];
                    
                    $personel[] = [
                        
                        'name'      => $worker->name,
                        
                        'total'     => Notes::find()->where($worker_id)->andWhere($dates)->count(),
                        
                        'done'      => Notes::find()->where($worker_id)->andWhere($dates)->andWhere( ["status" => 2 ] )->count(),
                        
                        'profit'    => Notes::find()->where($worker_id)->andWhere($dates)->andWhere( ["status" => 2 ] )->sum('price'),
                        
                    ];
                    
                    
                }
            

                $uslugi = Services::find()->where($user)->all();
            
                //var_dump($services);
            
                foreach($uslugi as $service){
                    
                    $name = ['service' => $service->name];
                    
                    $services[] = [
                        
                        'name'      => $service->name,
                        
                        'total'     => Notes::find()->where($name)->andWhere($dates)->count(),
                        
                        'done'      => Notes::find()->where($name)->andWhere($dates)->andWhere( ["status" => 2 ] )->count(),
                        
                        'profit'    => Notes::find()->where($name)->andWhere($dates)->andWhere( ["status" => 2 ] )->sum('price'),
                        
                    ];
                    
                    
                }
            
            
                $general = [
                    
                    'total' => Notes::find()->where($user)->andWhere($dates)->count(),
                    
                    'done'  => Notes::find()->where($user)->andWhere($dates)->andWhere( ["status" => 2 ] )->count(),
                    
                    'admin' => Notes::find()->where($user)->andWhere($dates)->andWhere( ["admin" => 1 ] )->count(),
                    
                    'client' => Notes::find()->where($user)->andWhere($dates)->andWhere( ["admin" => 0 ] )->count(),
                    
                    'profit' => Notes::find()->where($user)->andWhere($dates)->andWhere( ["status" => 1 ] )->sum('price'),
                    
                ];
            
                echo json_encode([
                    
                    'data'      => $data,
                    'from'      => date('d-m-Y', $startUnix),
                    'to'        => date('d-m-Y', $endUnix),
                    'p'         => $period,
                    
                    'tick'      => $i,
                    'chart'     => $chart,
                    
                    'general'   => $general,
                    'staff'     => $personel,
                    'services'  => $services
                
                ]);
            
			}
						
	}
?>