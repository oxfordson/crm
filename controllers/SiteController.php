<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

use yii\web\User;

class SiteController extends Controller {
    
		public function actionIndex() {	

            $this->layout = 'landing';
            
            return $this->render('index');
            
        }
    
    
        public function actionError() {	
            
            $this->layout = 'blank';
            
            return $this->render('error');
            
        }
    
    
}
