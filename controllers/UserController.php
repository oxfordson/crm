<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

use yii\web\User;

use app\models\Users;


class UserController extends Controller
{

	public function actionIndex()
		{
        
            $this->layout = 'blank';

			if( Yii::$app->user->identity )
				{

                    header( 'Refresh: 0; url=/journal/' );
                
				}					
			else
				{
					return $this->render('auth');	
				}
        
		}
		
	public function actionReg()
		{
        
            $this->layout = 'landing';
        
			return $this->render('reg');
		}
    
    public function actionHelp() 
        {



        }
	
	public function actionRegistrate()
		{

            $r = Yii::$app->request;
        
            $new_company = new Users(); 

            $new_company->mail = $r->post('mail');
            $new_company->password = $r->post('password');
        
            $new_company->phone = htmlspecialchars( $r->post('phone') );
        
            $new_company->name = htmlspecialchars($r->post('name') );
            $new_company->city = htmlspecialchars($r->post('city') );
        
            $new_company->address = htmlspecialchars($r->post('address') );
        
            $new_company->site = htmlspecialchars($r->post('site') );
        
            $valid = $new_company->save();
        
            if($valid){
            
                Yii::$app->user->login($new_company, 3600*24*30);
                
            }
        
            Yii::$app->mailer->compose('registration', ['data' => $new_company])
                ->setFrom('info@ionlion.ru')
                ->setTo($new_company->mail)
                ->setSubject("Регистрация в сервисе ionlion.ru")
                ->send();
        
            echo json_encode(['ok' => $valid ]);

	}
	
	public function actionEmail($email)
		{
			$mail = Users::findOne(['mail' => $email]);
        
            $mail = ($mail) ? true : false;
			
            echo json_encode(['user' => $mail]);
		}
    
	public function actionChangepass($mail)
		{
        
			$user = Users::findOne(['mail' => $mail]);
        
            $result = false;
        
            if($user) {
                
                $l = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
                      'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
                      'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
                
                shuffle($l);
                
                $user->password = $l[0].$l[1].$l[2].$l[3].$l[4].$l[5].$l[6].$l[7];
                
                $user->save();
                
                
                Yii::$app->mailer->compose('password', ['password' => $user->password])
                    ->setFrom('info@ionlion.ru')
                    ->setTo($user->mail)
                    ->setSubject("Ваш новый пароль для ionlion")
                    ->send();
                
                $result = true;
                
            }
			
            echo json_encode(['ok' => $result]);
        
		}
	
	public function actionLogin()
		{
        
            $ok = false;
        
			if(!empty($_POST['mail']) && !empty($_POST['password']))
				{
					$identity = Users::findOne(['mail' => $_POST['mail'], 'password' => $_POST['password'] ]);
                
					if($identity)
						{
                        
                            Yii::$app->user->login($identity, 3600*24*30);
                        
                            $ok = true;
                        
						}
                
                }
        
            echo json_encode(['ok' => $ok]);
        
		}
    
	public function actionLogout()
		{
            
            Yii::$app->user->logout();
            
            header( 'Refresh: 0; url=/' );
        
        }

}