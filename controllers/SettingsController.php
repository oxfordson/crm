<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

use app\models\Users;

class SettingsController extends Controller
	{
	
	
        public function behaviors() {
            
            return [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ],
            ];
            
        }
			
			
/*-----------------------------------------------------------------------------------------------------------------------------------*/	
//вывод таблицы мастеров
/*-----------------------------------------------------------------------------------------------------------------------------------*/		

		
		public function actionIndex()
			{	

				return $this->render('settings');
            
			}
    
    
		public function actionGetinfo()
			{	
				$user = Users::find()->where([ 'id' => Yii::$app->user->id ])->asArray()->one();
            
                unset($user['password']);
            
                echo json_encode($user);
			}
			
			
			
			
/*-----------------------------------------------------------------------------------------------------------------------------------*/	
//сохранение настроек аккаунта
/*-----------------------------------------------------------------------------------------------------------------------------------*/		

					
		public function actionSavemail($email)
			{			
                $user = Users::findOne(Yii::$app->user->id);
            
                $user->mail = $email;
            
                echo json_encode(['ok' => $user->save() ]);
            
			}
    

		public function actionSavephone()
			{			
                $user = Users::findOne(Yii::$app->user->id);

                $user->phone = $_POST['phone'];
            
                echo json_encode(['ok' => $user->save() ]);
            
			}
    
    
		public function actionNewpass()
			{			
            
                $user = Users::findOne(['id' => Yii::$app->user->id, 'password' => $_POST['oldpass'] ]);
            
                $response = false;

                if($user) {
                    
                    $user->password = $_POST['password'];
                    
                    $response = $user->save();
    
                }
            
                echo json_encode(['ok' =>  $response]);
    
            
            
			}
			
			
/*-----------------------------------------------------------------------------------------------------------------------------------*/	
//сохранение информации
/*-----------------------------------------------------------------------------------------------------------------------------------*/		

					
		public function actionSaveinfo()
			{
            
                $r = Yii::$app->request;

                $user = Users::findOne(Yii::$app->user->id);
            
                $user->name = htmlspecialchars( $r->post('name') );

                $user->city = htmlspecialchars( $r->post('city') );
                $user->address = htmlspecialchars( $r->post('address') );
                $user->about = htmlspecialchars( $r->post('about') );
            
                $user->site = htmlspecialchars( $r->post('site') );
            
                $user->mail = 'oxfordson@mail.ru';

				
                $user->publicphone = htmlspecialchars( $r->post('publicphone') );		
        
                echo json_encode(['ok' => $user->save() ]);
            
			}
				
/*-----------------------------------------------------------------------------------------------------------------------------------*/	
//отображение прайс-листа
/*-----------------------------------------------------------------------------------------------------------------------------------*/				
		public function actionPrice()
			{
            
                $salons = Autorized_salons::model()->findByAttributes(array('id' => $_COOKIE['login']));				
                $csv = json_decode($salons->price, true);
                $d = 0;
                $c = 1000;

                $service_ids = 1;

                foreach($csv as $key => $head)
                    {
                        echo "<p style='font-size: 20px;'>

                                    <span class='GroupName' onclick='slide($d);'>$key</span>


                                    <i class='fa fa-plus' title='Добавить подраздел' onclick='SubGroupAdd($d, this)'></i>

                                    <i class='fa fa-pencil' title='Редактировать раздел' onclick='GroupEdit($d, this)'></i> 

                                    <i class='fa fa-trash' title='Удалить' style='color: red' onclick='GroupDelete($d, this)'></i>                        

                                </p><ul style='display:none;' class='$d'>";

                        foreach($head as $key => $head2)
                            {
                                echo "<li>
                                            <div class='pri' id='$c'>

                                                <span class='GroupName' onclick='slide($c);'>$key</span>


                                                <i class='fa fa-plus' title='Добавить услугу' onclick='ServiceAdd($c)'></i>

                                                <i class='fa fa-pencil' title='Редактировать подраздел' onclick='SubGroupEdit($c, this)'></i>

                                                <i class='fa fa-trash' title='Удалить' style='color: red' onclick='SubGroupDelete($c)'></i>

                                            </div>
                                        
                                            <div style='display:none; padding-left: 30px;' class='$c'>";

                                                    foreach($head2 as $service)
                                                        {

                                                            list($name, $price, $time) = $service;

                                                            $m = $time*15;
if (!$time)
                                                            $time=0;
                                                            echo "<span class='service' id='service-$service_ids' >$name

                                                                <div class='tsena'>$price руб, $m мин 

                                                                    <i class='fa fa-pencil' title='Редактировать' onclick=\"ServiceEdit($service_ids, $price, '$name', $time)\"></i> 
                                                                    <i class='fa fa-trash' title='Удалить' style='color: red' onclick='ServiceDelete($service_ids)'></i>

                                                                </div>

                                                            </span>";

                                                            $service_ids++;

                                                        }

                                                    $c++;

                                    echo "</div></li>";
                                
                            }
                        echo "</ul>";
                        $d++;
                    }
					
			}
    
    
/*-----------------------------------------------------------------------------------------------------------------------------------*/	
//Редактирование прайс-листа
/*-----------------------------------------------------------------------------------------------------------------------------------*/	   
    
  		public function actionEditService($id = 'default', $price = 'default', $name = '', $time = 'default', $action = 0)
			{
            
                $salons = Autorized_salons::model()->findByAttributes(array('id' => $_COOKIE['login']));				
                $csv = json_decode($salons->price, true);

                $service_ids = 1;
            
				$group_id = 0;
				$subgroup_id = 1000;
            
                $name = substr( strip_tags($name), 0, 50 );
                $time = substr( strip_tags($time), 0, 2 );
                $price = substr( strip_tags($price), 0, 4 );
            
                if($action === 'GroupAdd')
                        {
                            $csv['Новый подраздел'] = array();
                    
                            echo count($csv)-1;
                        }

                foreach($csv as $key1 => $head)
                    {
                        
                        $SubgroupInGroup = 0;
                    
                    
                        if($action == 'GroupEdit' && !empty($name) && $group_id == $id && $name != $key1)
                                {

                                    $csv = array_insert($csv, $group_id, $name, $head);

                                    unset($csv[$key1]);
                                    break;

                                }
                    
                        if($action == 'GroupDelete' && $group_id == $id)
                                {

                                    unset($csv[$key1]);
                                    break;

                                }
                    
                        if($action == 'SubGroupAdd' && $group_id == $id)
                                {

                                    $csv[$key1]['Новый подраздел'] = array();
                                    break;

                                }
                        
                    

                        foreach($head as $key2 => $head2)
                            {
                            
                            
                                if( $action == 'SubGroupEdit' && !empty($name) && $subgroup_id == $id && $name != $key2)
                                        {

                                            $subg = array_insert($csv[$key1], $SubgroupInGroup, $name, $head2);

                                            unset($subg[$key2]);

                                            $csv[$key1] = $subg;

                                            break 2;

                                        }

                                if( $action == 'SubGroupDelete' && $subgroup_id == $id)
                                        {

                                            unset($csv[$key1][$key2]);
                                            break 2;

                                        }
                            
                                if( $action == 'ServiceAdd' && $subgroup_id == $id)
                                        {

                                            $csv[$key1][$key2][] = array('Новая услуга', 0, 1);
                                    
                                            echo $group_id;
                                            break 2;

                                        }

                                foreach($head2 as $key3 => $service)
                                    {
                                        
                                        if(isset($id) && $service_ids == $id)
                                            {
                                            
                                                if( !empty($name) && is_numeric($price) && is_numeric($time) )
                                                    {

                                                        $csv[$key1][$key2][$key3][0] = $name;
                                                        $csv[$key1][$key2][$key3][1] = $price;
                                                        $csv[$key1][$key2][$key3][2] = $time;


                                                        $m = $time*15;

if ($time == '')
                                                            $time='0';
                                                        echo "$name <div class='tsena'>$price руб, $m мин 

                                                                <i class='fa fa-pencil' title='Редактировать' onclick=\"ServiceEdit($service_ids, $price, '$name', $time)\"></i> 
                                                                
                                                                <i class='fa fa-trash' title='Удалить' style='color: red' onclick='ServiceDelete($service_ids)'></i>

                                                            </div>";
                                                    }
                                                elseif($action == 'delete')
                                                    {
                                                        unset($csv[$key1][$key2][$key3]);
                                                    }


                                            }
                                        $service_ids++;
                                    
                                    }
                            
                                $SubgroupInGroup++;
                                $subgroup_id++;
                            }
                            
                        $group_id++;
                    
                    }
            
                    $new_price = json_encode($csv);
            
                    $salons = Autorized_salons::model()->findByAttributes(array('id' => $_COOKIE['login']));
                    $salons->price = $new_price;					
                    $salons->save();
            
			} 
    

/*-----------------------------------------------------------------------------------------------------------------------------------*/	
//Экспорт прайс-листа
/*-----------------------------------------------------------------------------------------------------------------------------------*/	
    
		public function actionPriceExport()
			{
            
                $salons = Autorized_salons::model()->findByAttributes(array('id' => $_COOKIE['login']));				
                $priceAr = json_decode($salons->price, true);
            
                $url = 'data/'.$_COOKIE['login'].'/price.csv';
                $file = fopen($url, 'w+');
            
            
                $csv = array();

                foreach($priceAr as $key => $head)
                    {
                        fputcsv($file, array( iconv( "UTF-8", "Windows-1251", $key),null,null,null,null), ';' );

                        foreach($head as $key2 => $head2)
                            {
                                fputcsv($file, array(null,iconv( "UTF-8", "Windows-1251", $key2),null,null,null), ';' );

                                foreach($head2 as $service)
                                    {

                                        list($name, $price, $time) = $service;

                                        $m = $time*15;

                                        fputcsv($file, array(null,null,iconv( "UTF-8", "Windows-1251", $name), $price, $m), ';' );

                                    }

                            }
                    }
            
                fclose($file);
            
                header("Refresh: 0; url=/$url");
            
			}

				
/*-----------------------------------------------------------------------------------------------------------------------------------*/	
//загрузка прайс-листа
/*-----------------------------------------------------------------------------------------------------------------------------------*/			
		public function actionUpload()
			{	
				if(isset($_FILES['csv']))
					{
						if(preg_match("/(\.csv)$/", $_FILES['csv']['name']) )
							{
								move_uploaded_file($_FILES['csv']['tmp_name'], "data/".$_COOKIE['login']."/price.csv");
								
								$file = fopen("data/".$_COOKIE['login']."/price.csv","r");
								$a=1;
								
								$head = $head_2 = "Без раздела";
								
								while ($data = fgetcsv ($file, 1000, ";")) 
									{
										if(preg_match("/([A-zА-яЁё\d\s]){1,150}/i", $data[0]))
											{
												$head = strip_tags( iconv("Windows-1251", "utf-8", $data[0]) );
												$csv[$head] = array();
											}
										if(preg_match("/([A-zА-яЁё\d\s]){1,150}/i", $data[1]))
											{
												$head_2 = strip_tags( iconv("Windows-1251", "utf-8", $data[1]) );								
												$csv[$head][$head_2] = "";
											}
										if(preg_match("/([A-zА-яЁё\d\s]){1,150}/i", $data[2]) && is_numeric($data[3]))
											{	
												$name =  strip_tags( iconv("Windows-1251", "utf-8", $data[2]) );
												$prise =  $data[3];
												
												$duration = (is_numeric($data[4])) ? round($data[4]/15) : 1;
												
												$csv[$head][$head_2][$a] = array($name, $prise, $duration);
												$a++;
											}
									}
								
								$json = json_encode($csv);
								
								$salons = Autorized_salons::model()->findByAttributes(array('id' => $_COOKIE['login']));
								$salons->price = $json;					
								$salons->save();
													
								echo '<div>done</div>';
							}
						else{ echo '<div><p style="color: red">Не правильный формат!</p></div>'; }
					}
				else { echo '<div>Выбирите файл в формате CSV</div>'; }
					
			}
    
		
		public function actionCheckpass()
			{
				$user = Autorized_salons::model()->findByAttributes(array('id' => $_GET['id']));
				if(!empty($_GET['id']) && !empty($_GET['pass']) && isset($user) && $user->pass == $_GET['pass'])
					{
						echo("right");
					}
				else
					{
						echo("wrong");
					}
			}
	
				
	}
?>