<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

use app\models\Staff;
use app\models\Schedules;
use app\models\Notes;
use app\models\Sfw;
use app\models\Clients;
use app\models\Users;
use app\models\Services;

class JournalController extends Controller
	{
    

        public function behaviors() {
            
            return [
                'access' => [
                    'class' => AccessControl::className(),
                    'only' => ['index', 'get', 'drop', 'duration', 'remove', 'done'],
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ],
            ];
            
        }

		
		public function actionIndex()
			{	

				return $this->render('journal');
            
			}
    
		public function actionNote()
			{

                if( !empty( $_GET['id'] ) ){
                    
                    
                    if( Users::findOne( $_GET['id'] ) ) {
            
                        $this->layout = 'blank';

                        return $this->render('note');
                        
                    } else {
                        
                        header( 'Refresh: 0; url=/' );
                        
                    }
                    
                } else {
                    
                    header( 'Refresh: 0; url=/' );
                    
                }
            
			}
    
    
		public function actionGet()
			{
            
                if( empty($_POST['id']) ){
                    
                    $worker = Staff::findOne( [ 'user_id' => Yii::$app->user->id ]);
                    
                    $id = ($worker) ? $worker->id : NULL;
                    
                } else{
                    
                    $id = $_POST['id'];
                    
                }

                $unix = $startUnix = ( !empty( $_POST['unix'] ) ) ? $_POST['unix'] : Yii::$app->date->dateUnix('') ;
                                      
                $endUnix = $unix + 86400 * 3;
            
            
                $journal = [];
            
            
                while( $unix <= $endUnix ){
                    
                    $notes = Notes::find()->where([ 'worker_id' => $id, 'dateUnix' => $unix ])->asArray()->all();
                    
                    $hours = [];
                    
                    for($h = 0; $h <= 24; $h++){
                        
                        
                        $noteUnix = $unix + $h * 3600;
        
                        $hours[] = [
                            
                                    'textTime' => date('H:i', $noteUnix),
                                    'textEndTime' => date('H:i', $noteUnix + 3600),
                                    'unix' => $noteUnix,
                            
                                    'textDate' => Yii::$app->date->rusDate($noteUnix),
                            ];
                    }
                    
                    
                    $day = [
                            
                            'hours' => $hours, 
                            'unixDate' => $unix,
                            'date' => Yii::$app->date->rusDate($unix), 
                            'notes' => $notes,
                    ];
                
                    $journal[] = $day;
                    
                    
                    $unix += 86400;
                    
                    
                }
            
            
                $hoursList = [];

                for($time = 0; $time <= 23; $time++){ $hoursList[] = ($time < '10') ? "0$time:00" : "$time:00"; }
            
                $hoursList[] = "00:00";
            
            
                $staff = Staff::find()->where([ 'user_id' => Yii::$app->user->id ])->asArray()->all();
            
                $worker = Staff::find()->where( ['id' => $id] )->asArray()->one();
            
                echo json_encode([
                    
                    'hours' => $hoursList,
                    'days' => $journal,
                    'staff' => $staff,
                    'worker' => $worker,
                    'unix' => $startUnix,
                    'id' => Yii::$app->user->id,
                
                ]);
            
			}
    
    
		public function actionSave()
			{
            
                $r = Yii::$app->request;
            
            
                $unix = $r->post('unix');
            
                $note = Notes::findOne( ['unix' => $unix, 'worker_id' => $r->post('worker_id') ]);
            
                if(!$note) $note = new Notes();
            
                $endUnix = $unix + 3600;
            
                $duration = $endUnix - $unix;
            
                $position = date('G', $unix) * 48 + date('i', $unix) * 0.8;
            
                $height = $duration / 60 / 15 * 12;
            
            
                $note->name       =  $r->post('name');
                $note->service    =  $r->post('service');
                $note->price      =  $r->post('price');
                $note->phone      =  $r->post('phone');
                $note->comment    =  $r->post('comment');
            
                $note->unix       =  $unix;
                $note->endUnix    =  $endUnix;
                $note->duration   =  $endUnix - $unix;
                $note->dateUnix   =  Yii::$app->date->dateUnix($unix);
            
                $note->position   =  $position;
                $note->height     =  $height;
            
                $note->textTime      =  date('H:i', $unix);
                $note->textEndTime   =  date('H:i', $endUnix);
                $note->textDate      =  Yii::$app->date->rusDate($unix);
            
                $note->worker_id  =  $r->post('worker_id');
            
                if( !$r->post('srv_id') ){
                    
                    $note->admin = 1;
                    
                    $user_id = Yii::$app->user->id;
                    
                } else {
                    
                    $user_id = $r->post('user');
                    
                    $note->status = 0;
                    
                    $worker = Staff::findOne( $r->post('worker_id') );
                    
                    Yii::$app->mailer->compose('note', ['note' => $note, 'worker' => $worker])
                        ->setFrom('info@ionlion.ru')
                        ->setTo(Yii::$app->user->identity->mail)
                        ->setSubject("Новая запись ionlion.ru")
                        ->send();
                    
                }
            
                $note->user_id = $user_id;
            
                $note->save();
            
            
                $client = Clients::findOne( ['phone' => $r->post('phone') ]);
            
                if(!$client){
                    
                    $client = new Clients();
                    
                    $client->name = $r->post('name');
                    $client->phone = $r->post('phone');
                    $client->user_id = Yii::$app->user->id;
                    
                    $client->save();
                    
                }
            
            
                $user = Users::findOne( $user_id );
                
                $noteArray = Notes::find()->where( ['unix' => $unix, 'worker_id' => $r->post('worker_id') ])->asArray()->one();
            
                echo json_encode(['note' => $noteArray, 'url' => $user->site ]);
            
			}
    
		public function actionDrop()
			{
            
                $note = Notes::findOne( $_POST['id'] );
            
                $p = $_POST['position'] / 12 ;
            
                $newunix = $p * 15 * 60 + $_POST['date'];
            
                $endUnix = $newunix + $note->duration;
            
            
                $checktime = Notes::find()
                    
                           ->where(        ['worker_id' => $_POST['worker_id'] ])

                           ->andWhere(     ["and", "unix < $endUnix", "endUnix > $endUnix"])
                           ->orWhere(      ["and", "unix < $newunix", "endUnix > $newunix"])
                           ->orWhere(      ["and", "unix >= $newunix", "endUnix <= $endUnix"])

                           ->andWhere(     ["!=", "id", $_POST['id'] ])
                    
                 ->all();
                    
                
                if(!$checktime && $p >= 0){
                    
            
                    $note->unix        =  $newunix;
                    
                    $note->textTime      =  date('H:i', $newunix);
                    $note->textEndTime   =  date('H:i', $endUnix);
                    $note->textDate      =  Yii::$app->date->rusDate($newunix);

                    
                    $note->dateUnix    =  $_POST['date'];
                    
                    $note->position    =  $_POST['position'];
                    
                    
                    $note->endUnix = $endUnix;
                    
                    $note->save();
                    
                    $newnote = Notes::find()->where([ 'id' => $_POST['id'] ])->asArray()->one();

                    echo json_encode( ['note' => $newnote ]);
                    
                }
            
			}
    
		public function actionDuration()
			{
            
                $note = Notes::findOne( $_POST['id'] );
            
                $h = ( $_POST['height'] ) / 12 ;
            
                $duration = $h * 15 * 60;
            
                $endUnix = $note->unix + $duration;
            
            
                $checktime = Notes::find()
                    
                           ->where(        ['worker_id' => $_POST['worker_id'] ])

                           ->andWhere(     ["and", "unix < $endUnix", "endUnix >= $endUnix"])
                           ->orWhere(      ["and", "unix > $note->unix", "endUnix <= $endUnix"])

                           ->andWhere(     ["!=", "id", $_POST['id'] ])
                    
                 ->all();
                    

                if(!$checktime && ($note->dateUnix + 86400) >= $endUnix ){
                    
                    $note->height    =  $_POST['height'];
                    $note->duration  =  $duration;
                    $note->endUnix   =  $endUnix;
                    
                    $note->textEndTime   =  date('H:i', $endUnix);
                    
                    $note->save();
                    
                    $newnote = Notes::find()->where([ 'id' => $_POST['id'] ])->asArray()->one();

                    echo json_encode( ['note' => $newnote]);
                    
                }
            
			}
    
    
		public function actionRemove($id)
			{
            
				$note = Notes::findOne( $id );
            
                echo json_encode([ 'status' => $note->delete() ]);
            
			}
    
    
		public function actionStatus($status, $id)
			{
            
				$note = Notes::findOne( $id );
            
                $note->status = $status;
            
                $note->save();
            
			}
    
    
		public function actionList($id)
			{	
            
				$services = Services::find()->where(['user_id' => $id])->asArray()->All();
            
                echo json_encode([ 'list' => $services ]);
            
			}
    
    
		public function actionStaff($id, $srv)
			{
            
                $sfw = Sfw::find()->where(['service_id' => $srv])->all();
            
                $staff = [];
            
            

                foreach( $sfw as $srv ){
                    
                    $staff[] = Staff::find()->where(['id' => $srv->worker_id])->asArray()->one();
                    
                }
                    
            
                echo json_encode($staff);
            
			}
    
    
		public function actionHours($master, $date)
			{
                
                $unixDate = strtotime( $date );
            
                $schedule = Schedules::find()->where(['worker_id' => $master, 'unix' => $unixDate])->one();
            
                if (!$schedule) exit;
            
                $unix    = strtotime($schedule->from, $unixDate);
                $unixEnd = strtotime($schedule->to, $unixDate);
            
                $hours = [];
            
                while($unix < $unixEnd){
                    
                    $end = $unix + 1800;
                    
                    $checktime = Notes::find()
                    
                           ->where(        ['worker_id' => $master ])

                           ->andWhere(     ["and", "unix < $end", "endUnix > $end"])
                           ->orWhere(      ["and", "unix < $unix", "endUnix > $unix"])
                           ->orWhere(      ["and", "unix >= $unix", "endUnix <= $end"])
                    
                     ->all();
                    
                    $vacant = (!$checktime) ? true : false;
                        
                    $hours[] = [
                        
                        'time'   => date('H:i', $unix),
                        'unix'   => $unix,
                        'vacant' => $vacant
                        
                    ];
                    
                    
                    $unix += 1800;
                    
                }
            
                echo json_encode($hours);
            
            
            }   
    
						
	}
?>