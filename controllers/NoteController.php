<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

use app\models\Staff;
use app\models\Schedules;
use app\models\Notes;

class JournalController extends Controller
	{
	
		public function filters() 
			{
				return array(array('application.filters.AutorFilter + index, data, saveaccount, image, saveinfo, saveservices, services, upload, price, checkpass, EditService'));
			}
			
		
		public function actionIndex()
			{	

				return $this->render('journal');
            
			}
    
    
		public function actionGet()
			{
            
            
                if( empty($_POST['id']) ){
                    
                    $worker = Staff::findOne( [ 'user_id' => $_COOKIE['id'] ]);
                    
                    $id = $worker->id;
                    
                }
                else{
                    
                    $id = $_POST['id'];
                    
                }

                $unix = $startUnix = ( !empty( $_POST['unix'] ) ) ? $_POST['unix'] : Yii::$app->date->dateUnix('') ;
                                      
                $endUnix = $unix + 86400 * 3;
            
            
                $journal = [];
            
            
                while( $unix <= $endUnix ){
                    
                    $notes = Notes::find()->where([ 'worker_id' => $id, 'dateUnix' => $unix ])->asArray()->all();
                    
                    $hours = [];
                    
                    for($h = 0; $h <= 24; $h++){
                        
                        
                        $noteUnix = $unix + $h * 3600;
        
                        $hours[] = [
                            
                                    'textTime' => date('H:i', $noteUnix),
                                    'textEndTime' => date('H:i', $noteUnix + 3600),
                                    'unix' => $noteUnix,
                            
                                    'textDate' => Yii::$app->date->rusDate($noteUnix),
                            ];
                    }
                    
                    
                    $day = [
                            
                            'hours' => $hours, 
                            'unixDate' => $unix,
                            'date' => Yii::$app->date->rusDate($unix), 
                            'notes' => $notes,
                    ];
                
                    $journal[] = $day;
                    
                    
                    $unix += 86400;
                    
                    
                }
            
            
                $hoursList = [];

                for($time = 0; $time <= 23; $time++){ $hoursList[] = ($time < '10') ? "0$time:00" : "$time:00"; }
            
                $hoursList[] = "00:00";
            
            
                $staff = Staff::find()->where([ 'user_id' => $_COOKIE['id'] ])->asArray()->all();
            
                $worker = Staff::find()->where( ['id' => $id] )->asArray()->one();
            
                echo json_encode([
                    
                    'hours' => $hoursList,
                    'days' => $journal,
                    'staff' => $staff,
                    'worker' => $worker,
                    'unix' => $startUnix,
                
                ]);
            
			}
    
    
		public function actionSave()
			{
            
                $unix = $_POST['unix'];
            
                $note = Notes::findOne( ['unix' => $unix, 'worker_id' => $_POST['worker_id'] ]);
            
                if(!$note) $note = new Notes();
            
                $endUnix = $unix + 3600;
            
                $duration = $endUnix - $unix;
            
                $position = date('G', $unix) * 48 + date('i', $unix);
            
                $height = $duration / 60 / 15 * 12;
            
            
                $note->name       =  $_POST['name'];
                $note->service    =  $_POST['service'];
                $note->phone      =  $_POST['phone'];
            
                $note->worker_id  =  $_POST['worker_id'];
                $note->user_id    =  $_COOKIE['id'];
            
                $note->unix       =  $unix;
                $note->endUnix    =  $endUnix;
                $note->duration   =  $endUnix - $unix;
                $note->dateUnix   =  Yii::$app->date->dateUnix($unix);
            
                $note->position   =  $position;
                $note->height     =  $height;
            
                $note->textTime      =  date('H:i', $unix);
                $note->textEndTime   =  date('H:i', $endUnix);
                $note->textDate      =  Yii::$app->date->rusDate($unix);
            
                $note->save();
            
                
                $noteArray = Notes::find()->where( ['unix' => $unix, 'worker_id' => $_POST['worker_id'] ])->asArray()->one();
            
                echo json_encode(['note' => $noteArray ]);
            
			}
    
		public function actionDrop()
			{
            
                $note = Notes::findOne( $_POST['id'] );
            
                $p = $_POST['position'] / 12 ;
            
                $newunix = $p * 15 * 60 + $_POST['date'];
            
                $endUnix = $newunix + $note->duration;
            
            
                $checktime = Notes::find()
                    
                           ->where(        ['worker_id' => $_POST['worker_id'] ])

                           ->andWhere(     ["and", "unix < $endUnix", "endUnix > $endUnix"])
                           ->orWhere(      ["and", "unix < $newunix", "endUnix > $newunix"])
                           ->orWhere(      ["and", "unix >= $newunix", "endUnix <= $endUnix"])

                           ->andWhere(     ["!=", "id", $_POST['id'] ])
                    
                 ->all();
                    
                
                if(!$checktime && $p >= 0){
                    
            
                    $note->unix        =  $newunix;
                    
                    $note->textTime      =  date('H:i', $newunix);
                    $note->textEndTime   =  date('H:i', $endUnix);
                    $note->textDate      =  Yii::$app->date->rusDate($newunix);

                    
                    $note->dateUnix    =  $_POST['date'];
                    
                    $note->position    =  $_POST['position'];
                    
                    
                    $note->endUnix = $endUnix;
                    
                    $note->save();
                    
                    $newnote = Notes::find()->where([ 'id' => $_POST['id'] ])->asArray()->one();

                    echo json_encode( ['note' => $newnote ]);
                    
                }
            
			}
    
		public function actionDuration()
			{
            
                $note = Notes::findOne( $_POST['id'] );
            
                $h = ( $_POST['height'] ) / 12 ;
            
                $duration = $h * 15 * 60;
            
                $endUnix = $note->unix + $duration;
            
            
                $checktime = Notes::find()
                    
                           ->where(        ['worker_id' => $_POST['worker_id'] ])

                           ->andWhere(     ["and", "unix < $endUnix", "endUnix >= $endUnix"])
                           ->orWhere(      ["and", "unix > $note->unix", "endUnix <= $endUnix"])

                           ->andWhere(     ["!=", "id", $_POST['id'] ])
                    
                 ->all();
                    

                if(!$checktime && ($note->dateUnix + 86400) >= $endUnix ){
                    
                    $note->height    =  $_POST['height'];
                    $note->duration  =  $duration;
                    $note->endUnix   =  $endUnix;
                    
                    $note->textEndTime   =  date('H:i', $endUnix);
                    
                    $note->save();
                    
                    $newnote = Notes::find()->where([ 'id' => $_POST['id'] ])->asArray()->one();

                    echo json_encode( ['note' => $newnote]);
                    
                }
            
			}
    
    
		public function actionRemove($id)
			{
            
				$note = Notes::findOne( $id );
            
                echo json_encode([ 'status' => $note->delete() ]);
            
			}
    
						
	}
?>