<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

use app\models\Staff;
use app\models\Services;
use app\models\Schedules;
use app\models\Sfw;

class StaffController extends Controller
	{
	

        public function behaviors() {
            
            return [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ],
            ];
            
        }
			
		
		public function actionIndex()
			{	

				return $this->render('staff');
            
			}
    
    
		public function actionGet()
			{	
				$workers = Staff::find()->where([ 'user_id' => Yii::$app->user->id ])->asArray()->all();
            
                echo json_encode($workers);
			}
    
    
    //контроллеры добавления и редактирования мастеров
    
    
		public function actionAdd()
			{	
            
				$worker = new Staff(); 
                
                $worker->name = $_POST['name'];
            
                $worker->position = $_POST['position'];
                $worker->user_id = Yii::$app->user->id;
            
                $worker->image = 'img/worker.png';
            
                $worker->save();
            
                $neworker = Staff::Find()->where(['id' => $worker->id ])->asArray()->one();
            
                echo json_encode([ 'ok' => true, 'worker' => $neworker ]);
            
			}
    
		public function actionUpdate()
			{	
            
                $r = Yii::$app->request;
            
                $old = Staff::find()->where([ 'id' => $r->post('id') ])->asArray()->one();
            
            
				$worker = Staff::findOne( $r->post('id') );
            
                $worker->name = $r->post('name');
            
                $worker->position = $r->post('position');
            
                echo json_encode([ 'ok' => $worker->save(), 'old' => $old ]);
            
			}
    
		public function actionRemove($id)
			{	
            
				$worker = Staff::findOne( $id );
            
                Sfw::deleteAll([ 'worker_id' => $id ]);
            
                Schedules::deleteAll([ 'worker_id' => $id ]);
            
                if( $worker->image != 'img/worker.png') {

                    @unlink ( $worker->image );

                }
            
                echo json_encode([ 'ok' => $worker->delete() ]);
            
			}
    
    //контроллеры работы с услугами
    
		public function actionServices($id)
			{	
				$services = Services::find()->where([ 'user_id' => Yii::$app->user->id ])->asArray()->all();
                    
                $sfw = Sfw::find()->select('service_id')
                                  ->where(['worker_id' => $id])
                                  ->all();
            
                $sfw_ids = [];
                
                foreach($sfw as $service){
                    
                    $sfw_ids[$service->service_id] = true;
                    
                }
            
                echo json_encode(['list' => $services, 'sfw' => $sfw_ids]);
			}
    
		public function actionCheck($worker_id, $service_id)
			{	
				$sfw = Sfw::findOne(['worker_id' => $worker_id, 'service_id' => $service_id]);
                    
                if($sfw){
                    
                    $sfw->delete();
                }
                else{
                    
                    $check = new Sfw();
                    
                    $check->attributes = $_GET;
                    
                    $check->save();
                    
                }
            
			}
    
		public function actionImage()
			{
            
				if( isset($_FILES['img']['tmp_name']) )
					{
                    
						$types = ["", "gif", "jpeg", "png"];
                    
						list($width, $height, $type) = getimagesize($_FILES['img']['tmp_name']);
						
						if($type == 1 || $type == 2 || $type == 3 || $type == 6)
							{
							
								$f = $types[$type];
                            
                                $user = Yii::$app->user->id;
                            
								$url = "upload/$user/".date($user.'_YmdHis').".$f";	
                            
                            
                                if( !is_dir("upload/$user/") ){
                                    
                                    mkdir("upload/$user/", 777);
                                }
									
								if($width >= 430)
									{
										$func = 'imagecreatefrom'.$f;
										$old_img = $func($_FILES['img']['tmp_name']);
										
                                    
                                        if( $width < $height ){
                                            
                                            $new_height = 430 / ($width / $height);
                                            
                                            $new_width = 430;
                                            
                                            $top = ($height - $width) / 2;
                                            
                                            $left = 0;
                                            
                                            
                                        } else {
                                            
                                            $new_width = 430 / ($height / $width);
                                            
                                            $new_height = 430;
                                            
                                            $top = 0;
                                            
                                            $left = ($width - $height) / 2;
                                            
                                        }
                                    
										$new_img = imagecreatetruecolor(430, 430);
                                    
										imagecopyresampled($new_img, $old_img, 0, 0, $left, $top, $new_width, $new_height, $width, $height);
										
										$func = 'image'.$f;
								
										$func($new_img, $_FILES['img']['tmp_name']);												
									}
									
								move_uploaded_file($_FILES['img']['tmp_name'], $url);
                            
                                $worker = Staff::findOne($_POST['worker_id']);
                            
                                if( $worker->image != 'img/worker.png') {
                                    
                                    @ unlink ( $worker->image );
                                    
                                }
                            
                                $worker->image = $url;
                            
                                $worker->save();
                            
                                echo json_encode(['src' => $url, 'error' => 0]);
                            
							} else { 
                            
                                echo json_encode(['error' => "Изображение должно быть в формате JPG или PNG."]);
                        
                            }	
                    
					} else {
                    
                        echo json_encode(['error' => "Слишком большой файл изображения, максимальный размер файла не должен привышать 10 мегабайт."]);
                    
                    }
			}	
    
						
	}
?>