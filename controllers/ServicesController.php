<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

use app\models\Services;
use app\models\Sfw;

class ServicesController extends Controller
	{
	
	
        public function behaviors() {
            
            return [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ],
            ];
            
        }
			
		
		public function actionIndex()
			{	

				return $this->render('services');
            
			}
    
    
		public function actionGet()
			{	
				$services = Services::find()->where([ 'user_id' => $user = Yii::$app->user->id ])->asArray()->all();
            
                echo json_encode($services);
			}
    
    
		public function actionAdd()
			{	
            
				$service = new Services(); 
                
                $service->name = $_POST['name'];
            
                $service->price = $_POST['price'];
                $service->user_id = $user = Yii::$app->user->id;
            
                echo json_encode([ 'ok' => $service->save(), 'id' => $service->id ]);
            
			}
    
		public function actionUpdate()
			{	
            
                $r = Yii::$app->request;
            
                $old = Services::find()->where(['id' => $r->post('id') ])->asArray()->one();
            
            
				$service = Services::findOne( $r->post('id') );
            
                $service->name = $r->post('name');
            
                $service->price = $r->post('price');
            
                echo json_encode([ 'ok' => $service->save(), 'old' => $old ]);
            
			}
    
		public function actionRemove($id)
			{	
            
				$service = Services::find()->where(['id' => $id, 'user_id' => Yii::$app->user->id])->one();
            
                $service->delete();
            
                Sfw::deleteAll([ 'service_id' => $id ]);
            
                echo json_encode([ 'ok' => true ]);
            
			}
    
		public function actionList($id)
			{	
            
				$services = Services::find()->where(['user_id' => $id])->asArray()->All();
            
                echo json_encode([ 'list' => $services ]);
            
			}
    
		public function actionExport()
			{	
            
                $services = Services::find()->where(['user_id' => Yii::$app->user->id])->all();
            
                $csv = '';
            
                foreach($services as $service){
                    
                    $csv .= "$service->name;$service->price\n";
                    
                }
            
                $date = date('Y-m-d-h-i-s');
            
                return \Yii::$app->response->sendContentAsFile($csv, "price-$date.csv");
            
			}

        public function actionImport()
			{
            
				if(isset($_FILES['csv'])) {
                    
                    
						if(preg_match("/(\.csv)$/", $_FILES['csv']['name']) && $_FILES['csv']['size'] < 307200) {
								
								$csv = fopen($_FILES['csv']['tmp_name'], "r");
                            
                                Services::deleteAll([ 'user_id' => Yii::$app->user->id ]);
								
								while ( $row = fgetcsv ($csv, 1000, ";") ) {
                                    
                                    $service = new Services(); 

                                    $service->name    = $row[0];
                                    $service->price   = $row[1];
                                    $service->user_id = Yii::$app->user->id;
                                    
                                    $service->save();
                                    
								}
                            
                                $services = Services::find()->where(['user_id' => Yii::$app->user->id])->asArray()->All();
                            
                            
                                echo json_encode(['error' => false, 'services' => $services]);
								

                        } else { 
                            
                            echo json_encode(['error' => "Файл должен быть в формате CSV."]);
                        
                        }
                    
                }
            
            
            }
        
    
						
	}
?>