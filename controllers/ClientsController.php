<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

use app\models\Clients;

class ClientsController extends Controller
	{
	
	
        public function behaviors() {
            
            return [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ],
            ];
            
        }
			
		
		public function actionIndex()
			{	

				return $this->render('clients');
            
			}
    
    
		public function actionGet()
			{	
				$clients = Clients::find()
                    ->where([ 'user_id' => Yii::$app->user->id ])
                    ->orderBy('name')
                    ->asArray()
                    ->all();
            
                echo json_encode($clients);
			}
    
    
		public function actionAdd()
			{	
            
				$client = new Clients(); 
                
                $client->name = $_POST['name'];
            
                $client->phone = $_POST['phone'];
                $client->user_id = Yii::$app->user->id;
            
                echo json_encode([ 'ok' => $client->save(), 'id' => $client->id ]);
            
			}
    
		public function actionUpdate()
			{	
            
                $r = Yii::$app->request;
            
                $old = Clients::find()->where(['id' => $r->post('id') ])->asArray()->one();
            
            
				$client = Clients::findOne( $r->post('id') );
            
                $client->name = $r->post('name');
            
                $client->phone = $r->post('phone');
            
                echo json_encode([ 'ok' => $client->save(), 'old' => $old ]);
            
			}
    
		public function actionRemove()
			{	
            
				$client = Clients::findOne($_GET['id']);
            
                echo json_encode([ 'ok' => $client->delete() ]);
            
			}
    
		public function actionList($id)
			{	
            
				$clients = Clients::find()->where(['user_id' => $id])->asArray()->All();
            
                echo json_encode([ 'list' => $clients ]);
            
			}
    
						
	}
?>