<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

use app\models\Staff;
use app\models\Schedules;

class ScheduleController extends Controller
	{
	
	
        public function behaviors() {
            
            return [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ],
            ];
            
        }
			
		
		public function actionIndex()
			{	

				return $this->render('schedule');
            
			}
    
    
		public function actionGet()
			{
        
                $month = ( empty($_POST['month']) ) ? date('n') : $_POST['month'];
            
                $year = ( empty($_POST['year']) ) ? date('Y') : $_POST['year'];
            
                if( empty($_POST['id']) ){
                    
                    $worker = Staff::findOne( [ 'user_id' => Yii::$app->user->id ]);
                    
                    $id = ($worker) ? $worker->id : null ;
                    
                }
                else{
                    
                    $id = $_POST['id'];
                    
                }
            
            
            
                $startDate = date("$year-$month-01");               //Начальная дата
            
            
                $unix = strtotime($startDate);                      //unix начало
            
                $endUnix = $unix + date('t', $unix) * 86400;        //unix конец
            
            
                $startUnix = $unix;                        //копируем начальный unix
            
            
                $unix = $unix - 86400 * ( date('N', $unix) - 1 );               //добавляем дни из прошлого месяца
            
                $endUnix = $endUnix + 86400 * ( 8 - date('N', $endUnix) );      //добавляем дни из следующего месяца
            
            
                $calendar = array();

                while($unix < $endUnix){
                    
                    $day = [
                        
                        'date' => Yii::$app->date->rusDate($unix),
                        
                        'd' => date('j', $unix),
                        
                        'unix' => $unix,
                        
                        'time' => Schedules::find()->where([ 'worker_id' => $id, 'unix' => $unix ])->asArray()->one()
                                  
                    ];
                    
                    
                    if( date('n', $unix) != $month ){
                        
                        $day['othermonth'] = 'othermonth';
                        
                    }
                        
                        
                    $calendar[] = $day;
                    
                        
                    $unix += 86400;
                    
                }
            
            
                $autofill = ['datefrom' => date('d-m-Y', $startUnix), 'dateto' => date('d-m-Y', $startUnix + 86400*30), 'from' => '9:00', 'to' => '18:00', 'work' => 5, 'rest' => 2];
            
            
            
                $monthname = Yii::$app->date->monthName($month);
            
                $staff = Staff::find()->where([ 'user_id' => Yii::$app->user->id ])->asArray()->all();
            
                $worker = Staff::find()->where( ['id' => $id] )->asArray()->one();
            
            
                echo json_encode([
                    
                    'calendar' => $calendar, 
                    'month' => $month,
                    'monthname' => $monthname, 
                    'year' => $year,
                    'staff' => $staff,
                    'worker' => $worker,
                    'autofill' => $autofill
                
                ]);
            
			}
    
    
		public function actionSet()
			{	
            
                $time = Schedules::findOne( ['unix' => $_POST['unix'], 'worker_id' => $_POST['worker_id'] ]);
            
                if(!$time) $time = new Schedules(); 
            
                
                $time->from = $_POST['from'];
            
                $time->to = $_POST['to'];
            
                $time->date = $_POST['unix'];
            
            
                $time->worker_id = $_POST['worker_id'];
            
                echo json_encode([ 'status' => $time->save(), 'id' => $time->id ]);
            
			}
    
    
		public function actionUnset($id)
			{
            
				$time = Schedules::findOne( $id );
            
                echo json_encode([ 'status' => $time->delete() ]);
            
			}
    
    
		public function actionFill()
			{

                $unix = strtotime( $_POST['datefrom'] );
            
                $endUnix = strtotime( $_POST['dateto'] );
            
                $limit = $unix + 86400 * 100;
            

                if( $unix < $endUnix && $endUnix <= $limit) {
                    
                    
                    //Schedules::deleteAll(['>=', 'unix', $unix]);
                    Schedules::deleteAll(["and", "unix >= $unix", "unix <= $endUnix"]);
                    
                    //['>=', 'unix', $endUnix]
                    
                    $w = $r = 1;
                    
                    while( $unix <= $endUnix){
                        
                        
                        if( $w <= $_POST['work'] ){
                            
                            $time = new Schedules();
                            
                            $time->from = $_POST['from'];

                            $time->to = $_POST['to'];

                            $time->unix = $unix;
                            
                            $time->date = date('d-m-Y', $unix);
                            
                            $date = date('d-m-Y', $unix);


                            $time->worker_id = $_POST['id'];
                            
                            $time->save();
                            
                            $w++;
                            
                            $unix += 86400;
                            
                        }
                        else{
                            
                            if( $r <= $_POST['rest'] ) {
                            
                                $r++;
                                
                                $unix += 86400;
                            }
                            else{
                                
                                $w = $r = 1;
                                
                            }
                            
                        }
                        
                    }
                    
                }
            
                //echo json_encode([ 'status' => $time->delete() ]);
            
			}
    
    
    
    
    
						
	}
?>