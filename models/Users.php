<?php

namespace app\models;

use Yii;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class Users extends ActiveRecord implements IdentityInterface
    {
    
        public static function tableName()
        {
            return 'users';
        }
        
        public function rules()
            {
                return [
                    // the name, email, subject and body attributes are required
                    [ ['mail', 'password', 'phone'], 'required'],

                    // the email attribute should be a valid email address
                    ['mail', 'email'],
                    
                    ['mail', 'unique'],
                    
                    ['phone', 'match', 'pattern' => '/^[+][7] \([0-9]{3}\) [0-9]{3}-[0-9]{2}-[0-9]{2}$/i']
                ];
            }
    
    
        public static function findIdentity($id)
        {
            return static::findOne($id);
        }

        public static function findIdentityByAccessToken($token, $type = null)
        {
            return static::findOne(['access_token' => $token]);
        }

        public function getId()
        {
            return $this->id;
        }
    
        public function generateAuthKey() {
            
            $this->auth_key = Yii::$app->getSecurity()->generateRandomString();
            $this->save();

        }


        public function getAuthKey()
        {
            
            if(!$this->auth_key){
                
                $this->generateAuthKey();
                
            }
            
            return $this->auth_key;
        }

        public function validateAuthKey($authKey)
        {
            return $this->getAuthKey() === $authKey;
        }
    
    
    }