<?php

namespace app\models;

use yii\db\ActiveRecord;

class Services extends ActiveRecord
    {
        
        public function rules()
            {
                return [

                    [ ['name', 'price', 'user_id'], 'required'],
                    
                    ['price', 'integer']

                ];
            }
    
        public function getSfw($id)
            {
                return $this->hasMany(Sfw::className(), ['service_id' => 'id']);
            }
    
    
    }