<?php

namespace app\models;

use yii\db\ActiveRecord;

class Sfw extends ActiveRecord
    {
        
        public function rules()
            {
                return [

                    [ ['worker_id', 'service_id'], 'required'],

                ];
            }
    
        public function getStaff()
            {
                return $this->hasMany(Staff::className(), ['id' => 'worker_id']);
            }
    
    
    }