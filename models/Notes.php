<?php

namespace app\models;

use yii\db\ActiveRecord;

class Notes extends ActiveRecord
    {
        
        public function rules()
            {
                return [

                    [ ['name', 'service', 'worker_id', 'phone', 'unix'], 'required'],
                    
                    ['unix', 'integer'],
                    
                    ['phone', 'match', 'pattern' => '/^[+][7] \([0-9]{3}\) [0-9]{3}-[0-9]{2}-[0-9]{2}$/i']

                ];
            }
    
    
    }