<?php

namespace app\models;

use yii\db\ActiveRecord;

class Clients extends ActiveRecord
    {
        
        public function rules()
            {
                return [

                    [ ['name', 'phone', 'user_id'], 'required'],
                    
                    ['phone', 'match', 'pattern' => '/^[+][7] \([0-9]{3}\) [0-9]{3}-[0-9]{2}-[0-9]{2}$/i']

                ];
            }
    
    
    }