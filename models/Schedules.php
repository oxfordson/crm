<?php

namespace app\models;

use yii\db\ActiveRecord;

class Schedules extends ActiveRecord
    {
        
        public function rules()
            {
                return [

                    [ ['from', 'to', 'date', 'worker_id'], 'required'],
                    
                    ['from', 'match', 'pattern' => '#^(1?[0-9]|2[0-3]):[0,3]0$#'],
                    
                    ['to', 'match', 'pattern' => '#^(1?[0-9]|2[0-3]):[0,3]0$#']

                ];
            }
    
    
    }