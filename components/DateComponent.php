<?

namespace app\components;
 
 
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
 
class DateComponent extends Component{
    
     public function monthName($month)
         {
         
            $monthes = array("Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь");

            return $monthes[ $month - 1 ];
            
         }
    
     public function rusDate($date)
         {
         
            $monthes = array("января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря");

            $day = date('j', $date);
            $month = $monthes[ date('n', $date) - 1 ];
            $year = date('Y', $date);

            return "$day $month $year";
            
         }
    
     public function rusShortDate($date)
         {
         
            $monthes = array("янв.", "фев.", "мар.", "апр.", "мая", "июн.", "июл.", "авг.", "сен.", "окт.", "нояб.", "дек.");

            $day = date('j', $date);
            $month = $monthes[ date('n', $date) - 1 ];
            $year = date('Y', $date);

            return "$day $month";
            
         }
    
     public function dateUnix( $date )
         {
            
            if(!$date) $date = time();
         
            $text = date('d-m-Y', $date);

            return strtotime($text);
            
         }

}