<?php $this->title = 'Статистика'; ?>
   
    <div class="stats blur" ng-controller="Stats">

        <div class="row head">
          
            <div class="col-xs-6">
               
               <div class="switch">
                     
                      <span ng-click="period(1, $event)" ng-class="{active: p(1) }">День</span><!--
                   --><span ng-click="period(7, $event)" ng-class="{active: p(7) }">Неделя</span><!--
                   --><span ng-click="period(30, $event)" ng-class="{active: p(30) }">Месяц</span>
                   
               </div>
                
                
            </div>
           

            <div class="col-xs-3">

                <div class="group-input-left">
                    <div class="addon">с</div>
                    <input class="datefrom" ng-model="stats.from"/>
                </div>

            </div>

            <div class="col-xs-3">

                <div class="group-input-left">
                    <div class="addon">по</div>
                    <input class="dateto" ng-model="stats.to"/>
                </div>

            </div>
            
        </div>
           
        <div class="row graphic">
            
            <div d3chart data="stats.data" tick="stats.tick" class="chart"></div>
            
            <div class="chartTip"></div>
             
        </div>
          
       <div class="tabs">
           <span class="active" tab="general">Общая</span><span tab="staff">Персонал</span><span tab="services">Услуги</span>
       </div>
           
        <div class="row list">
            
            <div class="tab general">

                <div class="col-xs-3">
                   
                    <div class="stat" ng-class="{active: c('total')}" ng-click="chart('total')">
                       
                        <label>Всего записей</label>
                        {{ stats.general.total }}
                        
                    </div>
                    
                </div>

                <div class="col-xs-3">
                   
                    <div class="stat" ng-class="{active: c('done')}" ng-click="chart('done')">
                       
                        <label>Выполнено</label>
                        {{ stats.general.done }}
                        
                    </div>
                    
                </div>

                <div class="col-xs-3">
                   
                    <div class="stat" ng-class="{active: c('profit')}" ng-click="chart('profit')">
                       
                        <label>Прибыль</label>
                        {{ int(stats.general.profit) }} <i class="fa fa-rub"></i>
                        
                    </div>
                    
                </div>

                <div class="col-xs-3">
                   
                    <div class="stat" ng-class="{active: c('admin')}" ng-click="chart('admin')">
                       
                        <label>Записи админа</label>
                        {{ stats.general.admin }}
                        
                    </div>
                    
                </div>

                <div class="col-xs-3">
                   
                    <div class="stat" ng-class="{active: c('client')}" ng-click="chart('client')">
                       
                        <label>Онлайн-запись</label>
                        {{ stats.general.client }}
                        
                    </div>
                    
                </div>
                
            </div>
            
            <div class="tab staff hide">
              
                  <div class="row th">

                       <div class="col-xs-6 td">Имя</div>

                       <div class="col-xs-2 td text-right">Записи</div>

                       <div class="col-xs-2 td text-right">Выполнено</div>

                       <div class="col-xs-2 td text-right">Прибыль</div> 

                  </div>
                  
                  <div class="frame">

                      <div ng-repeat="worker in stats.staff">

                           <div class="col-xs-6 td">{{worker.name}}</div>

                           <div class="col-xs-2 td text-right">{{worker.total}}</div>

                           <div class="col-xs-2 td text-right">{{worker.done}}</div>

                           <div class="col-xs-2 td text-right">{{ int(worker.profit) }} <i class="fa fa-rub"></i></div>

                      </div>
                      
                  </div>
                
            </div>
            
            <div class="tab services hide">
               
                  <div class="row th">

                       <div class="col-xs-6 td">Название</div>

                       <div class="col-xs-2 td text-right">Записи</div>

                       <div class="col-xs-2 td text-right">Выполнено</div>

                       <div class="col-xs-2 td text-right">Прибыль</div> 

                  </div>
                  
                  <div class="frame">

                      <div ng-repeat="service in stats.services">

                           <div class="col-xs-6 td">{{service.name}}</div>

                           <div class="col-xs-2 td text-right">{{service.total}}</div>

                           <div class="col-xs-2 td text-right">{{service.done}}</div>

                           <div class="col-xs-2 td text-right">{{ int(service.profit) }} <i class="fa fa-rub"></i></div>

                      </div>
                      
                 </div>
                
            </div>
             
        </div>

    </div>


</div>

<script src="/js/jquery-ui-1.11.4/jquery-ui.min.js" type="text/javascript"></script>
<script src="/js/jquery-ui-1.11.4/datepicker-ru.js" type="text/javascript"></script>

<script src="/js/d3.min.js" type="text/javascript"></script>

<script src="/js/crm/stats.js" type="text/javascript"></script>




