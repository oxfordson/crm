<?php $this->title = 'Клиенты'; ?>

<h1>Клиенты</h1>

<div class="row workarea" ng-controller="Clients">

    <div class="col-xs-8">
       
        <div class="wrap search">
            <label><i class="fa fa-search"></i> &#160;Поиск по клиентам</label>
            <input ng-model="sFilter"/>
        </div>
        
        <div class="msg" ng-show="!filtered.length && clients[0]">
            По вашему запросу ни чего не найдено
        </div>
        
        <div class="msg" style="margin: 0;" ng-show="!clients[0] && isLoaded">
        
            <b>У вас пока нет ни одного клиента</b><br>
        
            <p>Клиенты добавляются автоматически при добавлении записи в журнале или онлайн-записи. Так же вы можете
            добавить клиента вручную на этой странице.</p>
            
            <p>Для изменения имени клиента или телефона просто кликните по полю.</p>
        
        </div>

        <div class="wrap clients" ng-show="filtered.length && isLoaded">
           {{ backup }}
            <div class="tab">
               
               <div class="one" ng-repeat="client in filtered = (clients | filter: sFilter)">
               
                    <div class="name" contentEditable="true" ng-model="name">
                        <input maxlength="30" spellcheck="false" ng-model="client.name" ng-blur="update($index)"/>
                    </div>
                    
                    <div class="phone">
                        <input ng-model="client.phone" ng-blur="update($index)"/>
                    </div>

                    <div class="delete" ng-click="remove($index)"><i class="fa fa-times"></i></div>

                </div>

            </div>

        </div>

    </div>
    
    <div class="col-xs-4">
        
        <div class="wrap addnew">
           
            <form name="newclient" novalidate>
              
                <h4>Новый клиент</h4>
              
                <br>

                <label>Имя</label>
                <input maxlength="20" name="name" ng-model="client.name" required/>
               
                <div ng-show="newclient.$submitted || newclient.name.$touched">
                    <div ng-show="newclient.name.$error.required" class="error"><i class="fa fa-exclamation-circle"></i>введите имя клиента</div>
                </div>
               
                <label>Телефон</label>
                
                <input class="phone" name="phone" ng-model="client.phone" required/>
               
                <div ng-show="newclient.$submitted || newclient.phone.$touched">
                    <div ng-show="newclient.phone.$error.required" class="error"><i class="fa fa-exclamation-circle"></i>введите телефон</div>
                </div>
               
                <input type="submit" ng-click="newclient.$valid && addClient(client)" class="button" value="Добавить"/>
               
           </form>
            
        </div>
        
    </div>


</div>


<script src="/js/crm/clients.js" type="text/javascript"></script>

<script type="text/javascript" src="/js/phoneformat.js"></script>