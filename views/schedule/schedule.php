
<?php $this->title = 'Графики работы'; ?>

    <div class="schedule" ng-controller="Schedule">

    <div ng-show="!schedule.worker && isLoaded" class="msg" style="margin: 25px 0">

            <b>У вас еще не добавленно ни одного сотрудника</b><br>
        
            <p>Для того, что бы заполнять графики работы ваших сотрудников, вам нужно сперва добавить их в разделе «Персонал»</p>
        
    </div>

    <div class="row" ng-show="schedule.staff">
    
        <div class="row head">
           
            <div class="col-xs-3">
               
                <div class="dropbut" m-dropbox=".staff">
                   
                    {{ schedule.worker.name }}
                    
                    <i class="fa fa-angle-down"></i>
                    
                </div>
                
                <ul class="dropbox staff">
                
                    <li ng-repeat="worker in schedule.staff">
                        <a href="/schedule/?id={{ worker.id }}">{{ worker.name }}</a>
                    </li>
                    
                </ul>
                
            </div>
            
            <div class="col-xs-5 month">
               
                <i class="fa fa-angle-left" ng-click="month( parseInt(schedule.month) - 1)"></i>
                   
                    <span>{{ schedule.monthname }} {{schedule.year}}</span>
                    
                <i class="fa fa-angle-right" ng-click="month( parseInt(schedule.month) + 1 )"></i>
                
            </div>
            
            <div class="col-xs-4 text-right">
               
                <div class="button" onclick="modal('.autoSetTime')"><i class="fa fa-pencil"></i> Автозаполнение</div>
                
            </div>
            
        </div>
        
        <div class="blur">

            <div class="days-of-week" ng-show="schedule.worker">
                <span>Пн</span>
                <span>Вт</span>
                <span>Ср</span>
                <span>Чт</span>
                <span>Пт</span>
                <span>Сб</span>
                <span>Вс</span>
            </div>

            <div ng-repeat="day in schedule.calendar">

                <div class="day {{day.othermonth}}" ng-click="!day.time && pickDay($index)">

                    {{day.d}}

                    <div class="time" ng-show="day.time" ng-click="pickDay($index)">

                        {{ day.time.from + ' — ' + day.time.to }}

                    </div>
                    
                    <div ng-show="day.time" class="remove" ng-click="unsetTime($index)"><i class="fa fa-times"></i></div>

                </div>

            </div>
            
        </div>

    </div>
    
    <div class="popup setTime hide">

        <h4>Время работы — {{ time.date }}</h4>

        <div class="row">
            <div class="col-xs-4">

                <div class="group-input-left">
                    <div class="addon">c</div>
                    <div ng-bind="time.from" m-dropbox=".from"></div>

                    <ul class="dropbox choose from" mchoose="time.from">
                        <?
                            for($i = 0; $i <= 23; $i++){

                                echo "<li>$i:00</li><li>$i:30</li>";

                            }
                        ?>
                    </ul>

                </div>

            </div>

            <div class="col-xs-4">

                <div class="group-input-left">
                    <div class="addon">до</div>
                    <div ng-bind="time.to" m-dropbox=".to"></div>

                    <ul class="dropbox choose to" mchoose="time.to">
                        <?
                            for($i = 0; $i <= 23; $i++){

                                echo "<li>$i:00</li><li>$i:30</li>";

                            }
                        ?>
                    </ul>                       

                </div>

            </div>

            <div class="col-xs-4">

                <input type="submit" ng-click="setTime(time)" class="button" value="Сохранить"/>

            </div>

        </div>

    </div>
    

    <div class="popup autoSetTime hide">

        <form name="AutoFilling" novalidate>
          
            <div class="row">
              
                <h5>Период заполнения</h5>
               
                <div class="col-xs-6">
                   
                    <div class="group-input-left">
                        <div class="addon">с</div>
                        <input name="from" class="datefrom" ng-model="autofill.datefrom" date/>
                    </div>
                    
                </div>

                <div class="col-xs-6">
                   
                    <div class="group-input-left">
                        <div class="addon">по</div>
                        <input name="from" class="dateto" ng-model="autofill.dateto" date/>
                    </div>
                    
                </div>
                
            </div>
            
            <div class="row">
              
                <h5>Чередование выходных и рабочих дней</h5>
               
                <div class="col-xs-6">
                   
                    <div class="group-input-left">
                        <div class="addon">Раб.</div>
                        <input type="number" min="1" name="work" ng-model="autofill.work" required onlynum/>
                    </div>
                    
                </div>

                <div class="col-xs-6">
                   
                    <div class="group-input-left">
                        <div class="addon">Вых.</div>
                        <input type="number" min="1" name="rest" ng-model="autofill.rest" required onlynum/>
                    </div>
                    
                </div>
                
            </div>
           
            <div class="row">
              
                <h5>Время работы</h5>
               
                <div class="col-xs-6">
                    
                    <div class="group-input-left">
                        <div class="addon">до</div>
                        <div ng-bind="autofill.from" m-dropbox=".auto-from"></div>

                        <ul class="dropbox choose auto-from" mchoose="autofill.from">
                            <?
                                for($i = 0; $i <= 23; $i++){

                                    echo "<li>$i:00</li><li>$i:30</li>";

                                }
                            ?>
                        </ul>                       

                    </div>
                    
                </div>

                <div class="col-xs-6">
                   
                    <div class="group-input-left">
                        <div class="addon">до</div>
                        <div ng-bind="autofill.to" m-dropbox=".auto-to"></div>

                        <ul class="dropbox choose auto-to" mchoose="autofill.to">
                            <?
                                for($i = 0; $i <= 23; $i++){

                                    echo "<li>$i:00</li><li>$i:30</li>";

                                }
                            ?>
                        </ul>                       

                    </div>
                    
                </div>
                
            </div>

            <input type="submit" ng-click="AutoFilling.$valid && fill(autofill)" class="button" value="Заполнить расписание"/>
            
       </form>

    </div>


</div>

<script src="/js/jquery-ui-1.11.4/jquery-ui.min.js" type="text/javascript"></script>
<script src="/js/jquery-ui-1.11.4/datepicker-ru.js" type="text/javascript"></script>

<script src="/js/crm/schedule.js" type="text/javascript"></script>




