<?php $this->title = 'Персонал'; ?>

<div class="row head">
    <div class="col-xs-9">
        <h1>Персонал</h1>
    </div>
    <div class="col-xs-3">
        <div class="button pull-right" onclick="modal('.addworker')"><i class="fa fa-user-plus"></i> Добавить сотрудника</div>
    </div>
</div>

<div class="staff" ng-controller="Staff">
    
    <div ng-show="!staff[0] && isLoaded" class="msg">
        
        <b>У вас еще не добавленно ни одного сотрудника</b><br>
        
        <p>Для того, что бы добавить сотрудника, нажмите кнопку вверху страницы и заполните имя и должность.</p>
        
        <p>Вы всегда сможете изменить информацию о сотруднике, просто кликнув.</p>
        
    </div>
   
    <div class="row" ng-show="staff[0]">
        
        <div class="col-xs-3" ng-repeat="worker in staff">
           
           <div class="wrap worker">
              
                <div class="image">
                    <img ng-src="/{{worker.image}}"/>
                    
                    <div class="uploadImg" ngf-select="upload($file, worker)"></div>
                </div>
                
                <div class="info">

                    <input maxlength="20" spellcheck="false" class="name" ng-model="worker.name" ng-blur="update($index)"/>

                    <input maxlength="20" spellcheck="false" class="position" ng-model="worker.position" ng-blur="update($index)"/>
                
                </div>
                
                <div class="settings">
                    <a href="#" ng-click="EditServices($index)" > Услуги </a>
                    |
                    <a href="/schedule/?id={{worker.id}}"> График </a>
                    |
                    <a ng-click="remove($index)" class="delete"> Удалить </a>
                </div>
               
               
           </div>
            

        </div>

    </div>
    
    <div class="popup addworker hide">

       <form name="nWorker" novalidate>

        <h4>Новый сотрудник</h4>

          <br>

           <label>Имя</label>
           <input maxlength="20" name="name" ng-model="worker.name" required/>

            <div ng-show="nWorker.$submitted || nWorker.name.$touched">
                <div ng-show="nWorker.name.$error.required" class="error"><i class="fa fa-exclamation-circle"></i>введите имя сотрудника</div>
            </div>

           <label>Должность</label>


            <input maxlength="20" name="position" ng-model="worker.position" required/>

            <div ng-show="nWorker.$submitted || nWorker.position.$touched">
                <div ng-show="nWorker.position.$error.required" class="error"><i class="fa fa-exclamation-circle"></i>укажите должность</div>
            </div>

           <input type="submit" ng-click="nWorker.$valid && addworker(worker)" class="button" value="Добавить"/>

       </form>

    </div>
    
    <div class="popup services hide">
      
        <h3>{{services.worker.name}}</h3>
       
        <div class="tab">
            <div ng-repeat="service in services.list">

                <div class="one {{ Relate(service.id) }}" ng-click="choseService(service.id)" id="{{service.id}}">

                    {{service.name}}

                </div>

            </div>
        </div>
        
        <div class="button" onclick="modalClose()">Сохранить</div>
        
    </div>
    
    
    <div class="popup uploadModal hide">
     
        <h4>Загрузка изображения</h4>
      
        <div class="upload" ng-show="!error">
            <div class="loadbar" style="width: {{progress}}"></div>
            <div class="percent">{{progress}}</div>
        </div>
        
        <div class="uploadError" ng-show="error">
            
            <div>{{error}}</div>
            
            <div class="button" ngf-select="upload($file, worker)">Выбрать файл</div>
            
        </div>
        
    </div>


</div>


<script src="/js/angular/ng-file-upload.min.js" type="text/javascript"></script>

<script src="/js/crm/staff.js" type="text/javascript"></script>




