<?php $this->title = 'Журнал'; ?>

<div class="journal" ng-controller="Journal">

    <div ng-show="!journal.worker && isLoaded" class="msg" style="margin: 35px 0;">
        
            <b>У вас еще не добавленно ни одного сотрудника</b><br>
        
            <p>Для того, что бы пользоваться журналом, вам нужно добавить сотрудников в разделе «Персонал»</p>
        
    </div>

    <div ng-show="journal.worker">
    
        <div class="row head">
           
            <div class="col-xs-3">
               
                <div class="dropbut" m-dropbox=".staff">
                   
                    {{ journal.worker.name }} 
                    
                    <i class="fa fa-angle-down"></i>
                    
                </div>
                
                <ul class="dropbox staff">
                
                    <li ng-repeat="worker in journal.staff">
                        <a href="/journal/?id={{ worker.id }}">{{ worker.name }}</a>
                    </li>
                    
                </ul>
                
            </div>

            <div class="col-xs-9 text-right">
              
                <div ng-click="changeDay( Int(journal.unix) - 86400)" class="button grey"><i class="fa fa-angle-left"></i></div> 
                    
                <div ng-click="changeDay( Int(journal.unix) + 86400)" class="button grey" style="margin: 0 10px 0 0"><i class="fa fa-angle-right"></i></div> 
                
               
                <div class="button grey openCalendar" resizable><i class="fa fa-calendar"></i></div>
                
                <div class="calendar hide"></div>
        
                
            </div>
            
        </div>
        
        <div class="blur row">
            
            <div class="col-xs-12">
                
                
                <div class="dates">
                    <div ng-repeat="day in journal.days" class="date">
                        {{ day.date }}
                    </div>
                </div>
                

                <div class="timetable">

                    <div class="hours">

                       <div class="date"> </div>

                       <div ng-repeat="hour in journal.hours track by $index" class="hour"> {{ hour }} </div>   

                    </div>

                    <div class="days">
                        

                        <div ng-repeat="day in journal.days" class="day">
                            

                            <div class="notes" jqyoui-droppable="{ onDrop: 'Drop' }" data-jqyoui-options="{tolerance: 'pointer'}" data-drop="true">
                                

                                <div ng-repeat="hour in day.hours" ng-click="$index < 24 && pickDay(hour, day)" class='cell'></div>
                                
                                <div ng-repeat="note in day.notes">

                                    <div class="note" data-jqyoui-options="dragOptions" 
                                        
                                                    jqyoui-draggable="dragParams" data-drag="true"
                                                    
                                                    ng-class="Status(note.status, true)"
                                                    
                                                    style="top: {{note.position}}px; height: {{note.height}}px" resizable>
                                         
                                        <div class="border">

                                            <div class="edit" ng-click="!dragParams.isDragging && pickDay(note)">{{ note.service }}</div>

                                            <div class="delete" ng-click="remove(day.notes, note)">
                                                <i class="fa fa-times"></i>
                                            </div>
                                            
                                        </div>
                                        

                                    </div>

                                </div>

                            </div>
                        
                        

                        </div>

                    </div> 

                </div>
            
            </div>

        </div>
    
    <div class="popup Note hide">

        <form name="aNote" class="forma" novalidate>
          
          
            <h4>Запись — {{ note.textDate }} c {{ note.textTime }} до {{ note.textEndTime }}</h4>
           
            <div class="row">
              
               <div class="col-xs-4">
                    <label>Название услуги</label>
               </div>

                <div class="col-xs-8">

                    <input type="text" ng-focus="autoComplete(note)" maxlength="40" autocomplete="off" name="service" ng-model="note.service" required>

                    <div ng-show="aNote.$submitted || aNote.service.$touched">
                        <div ng-show="aNote.service.$error.required" class="error"><i class="fa fa-exclamation-circle"></i>поле должно быть заполнено</div>
                    </div>
            
                    
                    <div class="autocomplete" class="hide">
                      
                       <ul>
                            <li ng-repeat="s in services.list | filter:note.service" ng-click="complete(s);">{{s.name}}</li>
                       </ul>
                       
                    </div>

                </div>
                
            </div>
             
            <div class="row">
              
               <div class="col-xs-4">
                    <label>Стоимость</label>
               </div>

                <div class="col-xs-4">
                    
                    <div class="group-input-right">
                        <input type="text" name="price" ng-model="note.price" required onlynum>
                        <div class="addon"><i class="fa fa-rub"></i></div>
                    </div>

                    <div ng-show="aNote.$submitted || aNote.price.$touched">
                        <div ng-show="aNote.price.$error.required" class="error"><i class="fa fa-exclamation-circle"></i>поле должно быть заполнено</div>
                    </div>

                </div>
                
                <div class="col-xs-4">
                    
                    <div ng-show="note.id" m-dropbox=".status" class="select" ng-bind="Status(note.status, false)"></div>
                    
                    <ul class="dropbox status">
                        
                        <li ng-click="setStatus(note, 0)">Новая запись</li>
                        <li ng-click="setStatus(note, 1)">Подтверждено</li>
                        <li ng-click="setStatus(note, 2)">Выполнено</li>
                        
                    </ul>
        
                </div>
                
            </div>
              
            <div class="row">
              
               <div class="col-xs-4">
                    <label>Имя клиента</label>
               </div>

                <div class="col-xs-8">

                    <input type="text" name="name" ng-model="note.name" required>

                    <div ng-show="aNote.$submitted || aNote.name.$touched">
                        <div ng-show="aNote.name.$error.required" class="error"><i class="fa fa-exclamation-circle"></i>поле должно быть заполнено</div>
                    </div>

                </div>
                
            </div>
              
            <div class="row">
              
               <div class="col-xs-4">
                    <label>Телефон</label>
               </div>

                <div class="col-xs-8">

                    <input type="text" class="phone" name="phone" ng-model="note.phone" required>

                    <div ng-show="aNote.$submitted || aNote.phone.$touched">
                        <div ng-show="aNote.phone.$error.required" class="error"><i class="fa fa-exclamation-circle"></i>поле должно быть заполнено</div>
                    </div>

                </div>
                
            </div>
              
            <div class="row">
              
                <div class="col-xs-4">
                    <label>Комментарий</label>
                </div>

                <div class="col-xs-8">

                    <textarea name="comment" ng-model="note.comment"></textarea>

                </div>
                
            </div>
               
            <div class="row">
                
                <div class="col-xs-12">

                    <input type="submit" ng-click="aNote.$valid && saveNote(note)" class="button" value="Сохранить"/>
                
                </div>

            </div>
            
       </form>

    </div>


</div>

<script src="/js/jquery-ui-1.11.4/jquery-ui.min.js" type="text/javascript"></script>
<script src="/js/jquery-ui-1.11.4/datepicker-ru.js" type="text/javascript"></script>

<script src="/js/angular/angular-dragdrop.min.js"></script>

<script type="text/javascript" src="/js/phoneformat.js"></script>

<script src="/js/crm/journal.js" type="text/javascript"></script>




