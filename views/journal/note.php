<?php $this->title = 'Онлайн-запись'; ?>

<div class="booking" ng-controller="Note">

    <div class="clt-note">
      
      
        <div class="success hide">

            <div class="row">

                <div class="col-xs-12">

                   <div class="title">Вы успешно записаны!</div>
                   
                   <div class="text">
                       Администратор свяжется с вами в ближайшее время. Точные дата и время записи будут
                        подтверждены после разговора с администратором.
                   </div>
                   
                   <a ng-show="url" ng-href="{{url}}" class="button">Вернуться на сайт организации</a>


                </div>


            </div>

        </div>
       
       <div class="steps">
           <div class="col-xs-3 step service active">Услуга<i></i></div>
           <div class="col-xs-3 master step">Мастер<i></i></div>
           <div class="col-xs-3 date step">Дата<i></i></div>
           <div class="col-xs-3 contacts step">Контакты</div>
       </div>
       
       <div class="wnds">
       
           <div class="step-1 hid">

                <div class="prc-lst">
                    <div ng-repeat="service in filtered = (services.list | filter: search)" class="srv" ng-click="chooseService(service)" ng-class="{checked: s(service.id)}">
                           
                        {{service.name}} — {{service.price}} р.

                    </div>
                </div>
                
                <div class="controls">
                    <div class="row">

                        <div class="col-xs-8">
                            <i class="fa fa-search"></i><input ng-model="search"/>
                        </div>
                        <div class="col-xs-4">

                                <div class="button pull-right" ng-click="master()">Продолжить</div>

                        </div>

                    </div>
                </div>

           </div>

           <div class="step-2 hide">
           
                <div class="msr-lst">
                    <div ng-repeat="master in masters" class="col-xs-4 msr" ng-class="{active: m(master.id)}" ng-click="chooseMaster(master.id)" id="msr-{{master.id}}">

                        <div class="img-wrap">
                            <img ng-src="/{{master.image}}"/>
                        </div>

                        <div class="name">{{master.name}}</div>

                        <div class="pstn">{{master.position}}</div>

                    </div>
                </div>
                
                <div class="controls">
                    <div class="button" ng-click="srv()">Вернуться</div>
                    <div class="button pull-right" ng-click="datetime()">Продолжить</div>
                </div>

           </div>
           
           <div class="step-3 hide">
           
                <div class="row">

                    <div id="calendar">

                        <div class="back" disabled>
                            <i class="fa fa-angle-left"></i>
                        </div>

                        <div class="days"></div>

                        <div class="forth">
                            <i class="fa fa-angle-right"></i>
                        </div>
                        
                    </div>
                      
                    <div class="hours">
                        
                        <div ng-repeat="hour in hours" class="col-xs-2">
        
                            <div class="hour"  ng-class="{vacant: hour.vacant, active: h(hour.unix)}"  ng-click="hour.vacant && chooseHour(hour.unix)">
                                {{hour.time}}
                            </div>
                            
                        </div>
                        
                        <div ng-show="!hours" class="msg">Мастер не работает в этот день</div>
                        
                    </div>
                       
                </div>
                       
                <div class="controls">
                    <div class="button" ng-click="master()">Вернуться</div>
                    <div class="button pull-right" ng-click="contacts()">Продолжить</div>
                </div>
                        
            </div>
           

           <div class="step-4 hide">
               
               <div class="cnts">
                
                    <form name="aNote">
                      
                        <div class="row">
                       
                            <div class="col-xs-7">

                                <div>

                                    <label>Имя *</label>

                                    <input type="text" name="name" ng-model="note.name" required>

                                    <div ng-show="aNote.$submitted || aNote.name.$touched">
                                        <div ng-show="aNote.name.$error.required" class="error"><i class="fa fa-exclamation-circle"></i>поле должно быть заполнено</div>
                                    </div>

                                </div>

                                <div>

                                    <label>Телефон *</label>

                                    <input type="text" class="phone" name="phone" ng-model="note.phone" required>

                                    <div ng-show="aNote.$submitted || aNote.phone.$touched">
                                        <div ng-show="aNote.phone.$error.required" class="error"><i class="fa fa-exclamation-circle"></i>поле должно быть заполнено</div>
                                    </div>

                                </div>

                                <div>

                                    <label>Комментарий</label>

                                    <textarea name="comment" ng-model="note.comment"></textarea>

                                </div>
                                
                            </div>
                        
                        <div class="controls">
                            <div class="button" ng-click="datetime()">Вернуться</div>
                            <div class="button pull-right" ng-click="aNote.$valid && send(note)">Записаться</div>
                        </div>
                        
                    </form>
                    
                </div>
          
           </div>
           
        </div>

    </div>

</div>

<script src="/js/jquery-ui-1.11.4/jquery-ui.min.js" type="text/javascript"></script>
<script src="/js/jquery-ui-1.11.4/datepicker-ru.js" type="text/javascript"></script>


<script type="text/javascript" src="/js/phoneformat.js"></script>

<script src="/js/crm/note.js" type="text/javascript"></script>




