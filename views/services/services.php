<?php $this->title = 'Прайс-лист'; ?>

<h1>Прайс-лист</h1>

<div class="row workarea" ng-controller="Services">

    <div class="col-xs-8">
       
        <div class="wrap search">
            <label><i class="fa fa-search"></i> &#160;Поиск по услугам</label>
            <input ng-model="sFilter"/>
        </div>
        
        <div class="msg" ng-show="!filtered.length && services[0]">
            По вашему запросу ни чего не найдено
        </div>
        
        <div class="msg" style="margin: 0;" ng-show="!services[0] && isLoaded">
        
            <b>У вас нет ни одной услуги</b><br>
        
            <p>Вы можете добавлять услуги по одной в окошке справа или загрузить весь прайс-лист целиком в формате CSV.</p>
            <p>Подробнее о создании и редактировании прайс-листа Вы можете прочитать в разделе справки.</p>
            
            <p>Для изменения названия услуги или ее стоимости просто кликните по полю.</p>
        
        </div>

        <div class="wrap services" ng-show="filtered.length && isLoaded">
           {{ backup }}
            <div class="tab">
               
               <div class="one" ng-repeat="service in filtered = (services | filter: sFilter)">
               
                    <div class="name" contentEditable="true" ng-model="name">
                        <input maxlength="30" spellcheck="false" ng-model="service.name" ng-blur="update($index)"/>
                    </div>
                    
                    <div class="price">
                        <input maxlength="6" ng-model="service.price" ng-blur="update($index)" onlynum/>
                        <i class="fa fa-rub"></i>
                    </div>

                    <div class="delete" ng-click="remove($index)"><i class="fa fa-times"></i></div>

                </div>

            </div>

        </div>

    </div>
    
    <div class="col-xs-4">
        
        <div class="wrap addnew">
           
            <form name="newservice" novalidate>
              
                <h4>Новая услуга</h4>
              
                <br>

                <label>Название</label>
                <input maxlength="30" name="name" ng-model="service.name" required/>
               
                <div ng-show="newservice.$submitted || newservice.name.$touched">
                    <div ng-show="newservice.name.$error.required" class="error"><i class="fa fa-exclamation-circle"></i>введите название</div>
                </div>
               
                <label>Цена</label>
                
                <div class="group-input-right">
                    <input maxlength="6" name="price" ng-model="service.price" required onlynum/>
                    <div class="addon"><i class="fa fa-rub"></i></div>
                </div>
               
                <div ng-show="newservice.$submitted || newservice.price.$touched">
                    <div ng-show="newservice.price.$error.required" class="error"><i class="fa fa-exclamation-circle"></i>укажите цену услуги</div>
                </div>
               
                <input type="submit" ng-click="newservice.$valid && addservice(service)" class="button" value="Добавить"/>
               
           </form>
            
        </div>
        
        <div class="wrap import">
           
            <h4>Прайс-лист.CSV</h4>
           
            <a class="button" ngf-select="import($file)">
               
                <i class="fa fa-upload"></i> Импорт
                
            </a>

            <a href="/services/export/" class="button grey">
                <i class="fa fa-download"></i> Экспорт
            </a>
            
        </div>
        
    </div>
    
    
    <div class="popup uploadModal hide">
     
        <h4>Импорт прайс-листа</h4>
      
        <div class="upload" ng-show="!error">
            <div class="loadbar" style="width: {{progress}}"></div>
            <div class="percent">{{progress}}</div>
        </div>
        
        <div class="uploadError" ng-show="error">
            
            <div>{{error}}</div>
            
            <div class="button" ngf-select="import($file)">Выбрать файл</div>
            
        </div>
        
    </div>


</div>


<script src="/js/angular/ng-file-upload.min.js" type="text/javascript"></script>
<script src="/js/crm/services.js" type="text/javascript"></script>