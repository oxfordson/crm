<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;


use yii\web\User;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html ng-app="crm">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>

        <title><?= Html::encode($this->title) ?></title>

        <?php //$this->head() ?>

        <link rel="stylesheet/less" type="text/css" href="/css/style.less" />

        <script src="/js/less.js" type="text/javascript"></script>

        <script type="text/javascript" src="/js/jquery-2.1.1.min.js"></script>

        <script type="text/javascript" src="/js/angular/angular.min.js"></script>

        <script type="text/javascript" src="/js/crm/main.js"></script>

    </head>

    <body>

        <div class="site">
            
            <header>
               
                <div class="container">
                    
                    <div class="row">
                        
                        
                        <div class="col-xs-3">
                            
                            <a href="/">
                                <img class="logo" src="/img/logo.png"/>
                            </a>
                            
                        </div>
                        
                        <div class="col-xs-9">
                           <div class="user">
                                <? include('views/layouts/client.php')?>
                            </div>
                        </div>
                        
                        
                    </div>
                    
                    
                </div>
                
            </header>
            
            <div class="container">

               <div class="row">

                   <div class="col-xs-2">

                        <nav class="menu">

                            <a href="/journal/"><i class="fa fa-book"></i><span>Журнал</span></a>
                            <a href="/staff/"><i class="fa fa-user"></i><span>Персонал</span></a>
                            <a href="/schedule/"><i class="fa fa-calendar"></i><span>Графики работы</span></a>
                            <a href="/services/"><i class="fa fa-bars"></i><span>Прайс-лист</span></a>
                            <a href="/stats/"><i class="fa fa-bar-chart"></i><span>Статистика</span></a>
                            <a href="/clients/"><i class="fa fa-phone"></i><span>Клиенты</span></a>
                            <a href="/settings/"><i class="fa fa-gear"></i><span>Настройки</span></a>

                        </nav>

                   </div>

                   <div class="col-xs-10 static" ng-cloak>

                        <?= $content ?>
                        
                   </div>

                </div>

            </div>
            
            <!--<footer class="footer">

            </footer>-->

        </div>

        <div class="loader"><img src="/img/load.gif"/></div>
        <div class="bg hide"> </div>

    </body>
</html>
