<?php


use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html ng-app="crm">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>

        <title><?= Html::encode($this->title) ?></title>

        <?php //$this->head() ?>

        <link rel="stylesheet/less" type="text/css" href="/css/style.less" />

        <script src="/js/less.js" type="text/javascript"></script>

        <script type="text/javascript" src="/js/jquery-2.1.1.min.js"></script>

        <script type="text/javascript" src="/js/angular/angular.min.js"></script>

        <script type="text/javascript" src="/js/crm/main.js"></script>

    </head>

    <body>

        <div class="site">
            
            <div class="container">

               <div class="row">

                    <?= $content ?>
                        
                </div>

            </div>

        </div>

        <div class="loader"><img src="/img/load.gif"/></div>
        <div class="bg hide"> </div>

    </body>
</html>
