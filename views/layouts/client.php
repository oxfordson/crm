<?

    if( !Yii::$app->user->identity ): 

?>


    <a href="/user/reg/" class="reg">Регистрация</a>
    <a href="/user/" class="log-in">Войти</a>


<? 

    else: 

        $notes = (new \yii\db\Query())

            ->select('*')
            ->from('notes')
            ->where(['user_id' => Yii::$app->user->id, 'status' => 0])
            ->andWhere(['>', 'unix', time() ])
            ->all();

        $num = count($notes);

?>
    
    <div class="dropbox notifications">
        <ul>
            <? 
                if( $num ):
            
                    foreach($notes as $note): 
            
            
                    $worker = (new \yii\db\Query())

                        ->select('image')
                        ->from('staff')
                        ->where(['id' => $note['worker_id'] ])
                        ->one();
            
            
            ?>

                        <a href="/journal/?id=<?= $note['worker_id'] ?>&unix=<?= $note['dateUnix'] ?>">
                           
                            <li class="newnote" id="note<?= $note['id'] ?>">
                               <div class="img">
                                    <img src="/<?= $worker['image'] ?>"/>
                               </div>
                               <div class="info">
                                    <div class="date">
                                        <?= $note['textDate'] ?> в <?= $note['textTime'] ?>
                                    </div>
                                    <?= $note['service'] ?>
                                </div>

                            </li>
                            
                        </a>
            <?
                    endforeach; 
            
                else:
            ?>
                <div class="empty">Нет новых записей</div>
            <?
                endif;
            ?>

        </ul>
    </div>
   
    <a href="/journal/">
       <div class="user-menu">
            Журнал
       </div>  
    </a>
   
    <a href="/user/help/">
       <div class="user-menu">
            Справка
       </div>  
    </a>

    <div class="user-menu" m-dropbox=".notifications">
        <i class="fa fa-bell"></i>
        <?  
        
            if($num) echo "<span class='number'>$num</span>";
        
        ?>
    </div>
    
    <div class="user-menu" m-dropbox=".account" style="min-width: 130px;">
        <?=Yii::$app->user->identity->mail?> <i class="fa fa-angle-down"></i>
    </div>
    
    <ul class="dropbox account">
        
        
        <a href="/settings/">Настройки</a>
        <a href="/user/logout/">Выйти</a>
        
    </ul>


<? endif; ?>