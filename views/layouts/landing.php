<?php



use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

use yii\web\User;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>

        <title><?= Html::encode($this->title) ?></title>

        <?php //$this->head() ?>

        <link rel="stylesheet/less" type="text/css" href="/css/style.less" />

        <script src="/js/less.js" type="text/javascript"></script>

        <script type="text/javascript" src="/js/jquery-2.1.1.min.js"></script>

        <script type="text/javascript" src="/js/angular/angular.min.js"></script>

        <script type="text/javascript" src="/js/crm/main.js"></script>

    </head>

    <body>

        <div class="site">
            
            <header>
               
                <div class="container">
                    
                    <div class="row">
                        
                        
                        <div class="col-xs-3">
                            
                            <a href="/">
                                <img class="logo" src="/img/logo.png"/>
                            </a>
                            
                        </div>
                        
                        <div class="col-xs-9">
                          
                           <div class="user">
                               
                                <? include('views/layouts/client.php')?>
                                
                           </div>
                            
                        </div>
                        
                        
                    </div>
                    
                    
                </div>
                
            </header>

            <?= $content ?>
            
            <footer>

               <div class="container">

                   <div class="row">

                        <div class="col-xs-8">

                            <div class="phone">
                                +7 (977) 809 98 90,
                                yanalve@ya.ru
                            </div>

                        </div>

                        <div class="col-xs-4">
                           <div class="info">
                                © ionlion 2013—<? echo date('Y') ?>
                           </div>
                        </div>

                    </div>

               </div>

            </footer>

        </div>

        <div class="loader"><img src="/img/load.gif"/></div>
        <div class="bg hide"> </div>

    </body>
</html>
