<?php $this->title = 'Авторизация'; ?>
    

<div ng-cloak>

    <div class="auth" ng-controller="Auth">

        <form novalidate name="auth">

            <h4>Вход в систему</h4>

            <div ng-show="error" class="err">Неверный e-mail или пароль</div>

            <p>E-mail</p>
            <input type="email" ng-model="user.mail" ng-change="reset()" name="mail" required>

            <p>Пароль</p>
            <input type="password" ng-model="user.password" ng-change="reset()" name="password" required>

            <div style="margin-top: 10px">
                <input type="submit" class="button" ng-click="auth.$valid && enter(user)" value="Авторизоваться"/>

                <a href="#" ng-click="changePass()" style="margin-left: 25px">Забыли пароль?</a>

            </div>

        </form>

    </div>

    <div class="cPass hide" ng-controller="Auth">

       <form novalidate name="passchange">

            <h4>Смена пароля</h4>

            <div class="form">

                <p>Введите ваш E-mail</p>
                <input type="email" class="input" ng-model="user.mail" ng-change="reset()" name="mail" required>
                <input type="submit" class="button" ng-click="passchange.$valid && newpass(user)" value="Отправить"/>

                <div ng-show="passchange.$submitted || passchange.mail.$touched">

                    <div ng-show="passchange.mail.$error.email || passchange.mail.$error.required" class="error">
                        <i class="fa fa-exclamation-circle"></i> введите корректный e-mail
                    </div>

                    <div ng-show="wrongmail && !passchange.mail.$error.email" class="error">
                        <i class="fa fa-exclamation-circle"></i> пользователя с введенным e-mail не существует
                    </div>

                </div>

            </div>
            <div class="success hide">
                <p>Ваш новый пароль для входа в ситему был выслан на адрес электронной почты <b>{{user.mail}}</b></p>
                <br/>
                <div class="button" ng-click="back()">Вернуться</div>
            </div>

        </form>

    </div>

</div>


<script type="text/javascript">

    app = angular.module('crm', []).controller('Auth', function($scope, $location, $http, $httpParamSerializerJQLike) {
        
        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
        
        $scope.error = false;
        
        $scope.reset = function(){
            
            $scope.error = false;
            $scope.wrongmail = false;
            
        }
        
        $scope.enter = function(user) {

                $http.post('/user/login/', $httpParamSerializerJQLike(user) ).then(function(response){
                    
                    if(response.data.ok) {
                        
                        $scope.error = false;
                        
                        window.location = '/journal/';
                        
                        
                        
                    } else {
                        
                        $scope.error = true;
                        
                    }
                    
                });

        };
        
        $scope.changePass = function(){
            
            $('.cPass').removeClass('hide');
            
            $('.auth').addClass('hide');
            
        }
        
        $scope.back = function(){
            
            $('.auth, .form').removeClass('hide');
            
            $('.cPass, .success').addClass('hide');
            
        }
        
        $scope.newpass = function(user) {

                $http.get('/user/changepass/?mail=' + user.mail ).then(function(response){
                    
                    if(response.data.ok) {
                        
                        $scope.wrongmail = false;
                        
                        $('.success').removeClass('hide');

                        $('.form').addClass('hide');

                    } else {
                        
                        $scope.wrongmail = true;
                        
                    }
                    
                });

        };
        
    });
    
</script>