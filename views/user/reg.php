<?php $this->title = 'Регистрация'; ?>

<div ng-app="crm">

    <div class="regform" ng-controller="RegController">

        <form class="forma" novalidate name="form" ng-cloak>

            <h2>Регистрация</h2>

            <div class="row">
                <div class="col-xs-4">
                    <label>E-mail *</label>
                </div>

                <div class="col-xs-8">

                    <input type="email" ng-model="user.mail" name="mail" required usermail>

                    <div ng-show="form.$submitted || form.mail.$touched">

                        <div ng-show="form.mail.$error.email || form.mail.$error.required" class="error">
                            <i class="fa fa-exclamation-circle"></i>введите корректный e-mail
                        </div>

                        <div ng-show="form.mail.$error.usermail && !form.mail.$error.email" class="error">
                            <i class="fa fa-exclamation-circle"></i>такой e-mail уже существует
                        </div>

                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-xs-4">
                    <label>Телефон *</label>
                </div>

                <div class="col-xs-8">

                    <input type="text" class="phone" ng-model="user.phone" name="phone" required>

                    <div ng-show="form.$submitted || form.phone.$touched">
                        <div ng-show="form.phone.$error.required" class="error">
                            <i class="fa fa-exclamation-circle"></i>введите корректный телефон
                        </div>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-xs-4">
                    <label>Веб-сайт</label>
                </div>

                <div class="col-xs-8">

                    <input type="url" name="site" ng-model="user.site">

                    <div ng-show="form.$submitted || form.site.$touched">
                        <div ng-show="form.site.$error.url" class="error"><i class="fa fa-exclamation-circle"></i>формат адреса http://domain.ru</div>
                    </div>

                </div>
            </div>

            <div class="row" style="padding: 50px 0 0 0">

                <div class="col-xs-4">
                    <label>Пароль *</label>
                </div>

                <div class="col-xs-8">

                    <input type="password" ng-model="user.password" name="password" required>

                    <div ng-show="form.$submitted || form.password.$touched">
                        <div ng-show="form.password.$error.required" class="error">
                            <i class="fa fa-exclamation-circle"></i>введите пароль
                        </div>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-xs-4">
                    <label>Повторите пароль *</label>
                </div>

                <div class="col-xs-8">

                    <input type="password" ng-model="user.passconfirm" name="passconfirm" required passmatch>

                    <div ng-show="form.$submitted || form.passconfirm.$touched">
                        <div ng-show="form.passconfirm.$error.required" class="error">
                            <i class="fa fa-exclamation-circle"></i>повторите пароль
                        </div>
                        <div ng-show="form.passconfirm.$error.pass && !form.passconfirm.$error.required" class="error">
                            <i class="fa fa-exclamation-circle"></i>пароли не совпадают
                        </div>
                    </div>

                </div>
            </div>

            <div class="row" style="padding: 50px 0 0 0">
                <div class="col-xs-4">
                    <label>Название компании</label>
                </div>

                <div class="col-xs-8">

                    <input type="text" ng-model="user.name" name="name">

                </div>
            </div>

            <div class="row">
                <div class="col-xs-4">
                    <label>Город</label>
                </div>
                <div class="col-xs-8">
                    <input type="text" ng-model="user.city" name="city">
                </div>
            </div>


            <div class="row">
                <div class="col-xs-4">
                    <label>Адрес</label>
                </div>
                <div class="col-xs-8">
                    <input type="text" ng-model="user.address" name="address">

                    <div>
                        <input type="submit" class="button" ng-click="send(user)" value="Зарегистрироваться" />
                    </div>

                </div>
            </div>


            <br>
            <br>
            <br>


        </form>

    </div>

</div>

<script type="text/javascript" src="/js/phoneformat.js"></script>

<script type="text/javascript" src="/js/crm/reg.js"></script>
