<?php $this->title = 'Настройки'; ?>
       
<h1>Настройки</h1>

<div class="wrap settings" ng-controller="Settings">

    <form class="forma" name="info" novalidate>

       <h2>Информация</h2>

       <div class="row">
           <div class="col-xs-3">
                <label>Название</label>
            </div>

            <div class="col-xs-9">

                <input type="text" name="name" ng-model="user.name">

                <!--<div ng-show="info.$submitted || info.name.$touched">
                    <div ng-show="info.name.$error.required" class="error"><i class="fa fa-exclamation-circle"></i>поле должно быть заполнено</div>
                </div>-->

            </div>
        </div>

       <div class="row">
            <div class="col-xs-3">
                <label>Тел. для клиентов</label>
            </div>
            <div class="col-xs-9">
                <input type="text" name="publicphone" class="phone" ng-model="user.publicphone">
            </div>
        </div>

       <div class="row">
           <div class="col-xs-3">
                <label>Веб-сайт</label>
            </div>

            <div class="col-xs-9">

                <input type="url" name="site" ng-model="user.site">

                <div ng-show="info.$submitted || info.site.$touched">
                    <div ng-show="info.site.$error.url" class="error"><i class="fa fa-exclamation-circle"></i>формат адреса http://domain.ru</div>
                </div>

            </div>
        </div>

       <div class="row">
            <div class="col-xs-3">
                <label>Описание</label>
            </div>
            <div class="col-xs-9">
                <textarea ng-model="user.about"></textarea>
            </div>
        </div>

       <div class="row">
            <div class="col-xs-3">
                <label>Город</label>
            </div>
            <div class="col-xs-9">
                <input type="text" name="city" ng-model="user.city">
            </div>
        </div>

       <div class="row">
            <div class="col-xs-3">
                <label>Адрес</label>
            </div>
            <div class="col-xs-9">
                <input type="text" name="adress" ng-model="user.address">

                <div><input type="submit" class="button" ng-click="info.$valid && send(user)" value="Сохранить"/></div>
            </div>
        </div>

    </form>

    <form class="forma" name="newmail" novalidate>

        <h2>E-mail</h2>

        <div class="row">

            <div class="col-xs-3">
                <label>Текущий E-mail</label>
            </div>

            <div class="col-xs-9">
                <p>{{user.mail}}</p>
            </div>

        </div>

        <div class="row">
            <div class="col-xs-3">
                <label>Новый E-mail</label>
            </div>
            <div class="col-xs-9">
                <input type="email" name="mail" ng-model="account.mail" required exists>

                <div ng-show="newmail.$submitted || newmail.mail.$touched">

                    <div ng-show="newmail.mail.$error.email || newmail.mail.$error.required" class="error">
                        <i class="fa fa-exclamation-circle"></i> введите корректный e-mail
                    </div>

                    <div ng-show="newmail.mail.$error.exists && !newmail.mail.$error.email" class="error">
                        <i class="fa fa-exclamation-circle"></i> такой e-mail уже существует
                    </div>

                </div>

                <div><input type="submit" ng-click="newmail.$valid && savemail(account)" class="button" value="Сохранить"/></div>

            </div>
        </div>

    </form>

    <form class="forma" name="newphone" novalidate>

        <h2>Телефон</h2>

        <div class="row">

            <div class="col-xs-3">
                <label>Текущий номер</label>
            </div>

            <div class="col-xs-9">
                <p>{{user.phone}}</p>
            </div>

        </div>

        <div class="row">

            <div class="col-xs-3">
                <label>Новый номер</label>
            </div>


            <div class="col-xs-9">

                <input type="text" name="phone" class="phone" ng-model="account.phone" required>

                <div ng-show="newphone.$submitted || newphone.phone.$touched">
                    <div ng-show="newphone.phone.$error.required" class="error">
                        <i class="fa fa-exclamation-circle"></i> введите корректный телефон
                    </div>
                </div>

                <input type="submit" ng-click="newphone.$valid && savephone(account)" class="button" value="Сохранить"/>

            </div>
        </div>

    </form>

    <form class="forma" name='newpass' novalidate>

        <h2>Пароль</h2>

        <div class="row">

            <div class="col-xs-3">
                <label>Старый пароль</label>
            </div>

            <div class="col-xs-9">
                <input type="password" name="oldpass" ng-model="account.oldpass" required exists>

                <div ng-show="newpass.$submitted || newpass.oldpass.$touched">
                    <div ng-show="newpass.oldpass.$error.required" class="error">
                        <i class="fa fa-exclamation-circle"></i> введите старый пароль
                    </div>
                    <div ng-show="newpass.oldpass.$error.exists" class="error">
                        <i class="fa fa-exclamation-circle"></i> неверный пароль
                    </div>
                </div>

            </div>

        </div>

        <div class="row">

            <div class="col-xs-3">
                <label>Новый пароль</label>
            </div>

            <div class="col-xs-9">
                <input type="password" name="password" ng-model="account.password" required>

                <div ng-show="newpass.$submitted || newpass.password.$touched">
                    <div ng-show="newpass.password.$error.required" class="error">
                        <i class="fa fa-exclamation-circle"></i> введите новый пароль
                    </div>
                </div>

            </div>
        </div>

        <div class="row">

            <div class="col-xs-3">
                <label>Повторите пароль</label>
            </div>


            <div class="col-xs-9">

                <input type="password" name="passconfirm" ng-model="account.passconfirm" required passmatch>

                <div ng-show="newpass.$submitted || newpass.passconfirm.$touched">
                    <div ng-show="newpass.passconfirm.$error.required" class="error">
                        <i class="fa fa-exclamation-circle"></i> введите новый пароль
                    </div>
                    <div ng-show="newpass.passconfirm.$error.match && !newpass.passconfirm.$error.required" class="error">
                        <i class="fa fa-exclamation-circle"></i> пароли не совпадают
                    </div>
                </div>

                <div><input type="submit" ng-click="newpass.$valid && savepass(account)" class="button" value="Сохранить"/></div>

            </div>
        </div>

    </form>

</div>


<script type="text/javascript" src="/js/phoneformat.js"></script>
<script src="/js/crm/settings.js" type="text/javascript"></script>





