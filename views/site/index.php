<?php

$this->title = 'Система онлайн записи ionlion';

?>

    <div class="banner">
        <div class="container">
            <div class="row">
                <div class="text">Управляй клиентами и сотрудниками из<br/> любой точки мира</div>

                <a href="/user/reg/"><div class="button">Начать пользоваться</div></a>
                
            </div>
        </div>
    </div>
    
    
    <div class="features">
        
        <div class="container">
            
            <div class="row">
                
                
                <div class="col-xs-6">
                    
                    <div class="row">
                        
                        <div class="col-xs-3">
                            <img src="/img/stats.png"/>
                        </div>
                        
                        <div class="col-xs-9">
                            <h3>Статистика</h3>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis similique illum itaque modi beatae porro rerum ipsam necessitatibus tenetur fuga!
                        </div>
                        
                    </div>
                    
                    <div class="row">
                        
                        <div class="col-xs-3">
                            <img src="/img/pencil.png"/>
                        </div>
                        
                        <div class="col-xs-9">
                            <h3>Онлайн-запись</h3>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis similique illum itaque modi beatae porro rerum ipsam necessitatibus tenetur fuga!
                        </div>
                        
                    </div>                   
                    
                </div>
                
                <div class="col-xs-6">
                    
                    <div class="row">
                        
                        <div class="col-xs-3">
                            <img src="/img/schedule.png"/>
                        </div>
                        
                        <div class="col-xs-9">
                            <h3>Графики сотрудников</h3>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis similique illum itaque modi beatae porro rerum ipsam necessitatibus tenetur fuga!
                        </div>
                        
                    </div>
                    
                    <div class="row">
                        
                        <div class="col-xs-3">
                            <img src="/img/contacts.png"/>
                        </div>
                        
                        <div class="col-xs-9">
                            <h3>Телефонная книга</h3>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis similique illum itaque modi beatae porro rerum ipsam necessitatibus tenetur fuga!
                        </div>
                        
                    </div>
                    
                </div>
                
                
                
            </div>
            
            
        </div>
        
        
    </div>
    
    
    
    
    
    
