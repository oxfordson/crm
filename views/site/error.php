<?php $this->title = 'Ошибка: 404'; ?>

<div class="error-404">
    
    <div class="col-xs-5">
        
        <img src="/img/lion-error.png"/>
        
    </div>
    
    <div class="col-xs-7">
        
        <h1>Ррр.. Эрррорр 404</h1>
        
        <p>Запрашиваемая страница не найдена, вам стоит<br/> проверить введенный адрес или просто вернуться<br/> на <a href="/">главную страницу </a> сайта.</p>
    </div>
    
</div>