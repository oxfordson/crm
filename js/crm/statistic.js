
/*-----------------------------------------------------------------------------------------------------------------------------------*/	
//статистика
/*-----------------------------------------------------------------------------------------------------------------------------------*/		
	
$(function()
	{
		window.mind = $('#startdate').val();
		window.maxd = $('#enddate').val();
		getst('salon_id', window.value, '', window.mind, window.maxd);
		
		$('input:checkbox').click(function()
			{
				if( $('input:checked').length > 50 && window.tariff != 'false')
					{
						$(this).removeAttr('checked');
						myAlert("Лимит рассылки 50 адресов.");
					}
			});		
	});
	
function getst(uslovie, value, v, mind, maxd) 
	{		
		$('#loarding').show('fade',100);
		$.getJSON("/statistic/getstat/?uslovie="+uslovie+"&value="+value+"&mind="+mind+"&maxd="+maxd+"&width="+screen.width, 
				function(result)
					{
						$('#allsum').html(result.allsum);
						$('#allkol').html(result.allkol);				
						$('#donesum').html(result.donesum);
						$('#uniclients').html(result.uniclients);				
						$('#donekol').html(result.donekol);
						$('#adminkol').html(result.adminkol);				
						$('#adminsum').html(result.adminsum);
						$('#clientkol').html(result.clientkol);				
						$('#clientsum').html(result.clientsum);
						$('#grafic').html(result.grafic);						
						
						window.uslovie = uslovie;
						window.value = value;
						$('#loarding').hide('fade',100);
					});
		
		$("#mastera li").switchClass("activmast","statmaster");
		$(v).switchClass("statmaster","activmast");
		
		$(".statbox").css("background","#fff");
		$(".statbox:first").css("background","#fffcb8");
		
	}
	
function getgrafic(v, params)
	{		
		$('#loarding').show('fade',100);
		$.getJSON("/statistic/getstat/?grafic="+params+"&uslovie="+window.uslovie+"&value="+window.value+"&mind="+window.mind+"&maxd="+window.maxd+"&width="+screen.width, 
				function(result)
					{
						$('#grafic').html(result.grafic);						
						$('#loarding').hide('fade',100);
					});
		
		$(".statbox").css("background","#fff");
		$(v).css("background","#fffcb8");
	}
	
function period()
	{
		window.mind = $('#startdate').val();
		window.maxd = $('#enddate').val();
		getst(window.uslovie, window.value, '', window.mind, window.maxd)
	}

/*-----------------------------------------------------------------------------------------------------------------------------------*/	
//клиенты
/*-----------------------------------------------------------------------------------------------------------------------------------*/
		
function getcl(uslovie, value, v) 
	{	
		$('#loarding').show('fade',100);
		$.ajax({url:"/clients/getclients/?uslovie="+uslovie+"&value="+value, 
				dataType: 'json',
				success: function(result)
					{
						$('#clientstab').html(result.clients);
						$('#tomail').html(result.tomail);
						
						window.uslovie = uslovie;
						window.value = value;
						
						$("#mastera li").switchClass("activmast","statmaster");
						$(v).switchClass("statmaster","activmast");
						$(".od:odd").css("background","#eaf0f2");
						$('#loarding').hide('fade',100);
						
						$('#sending, .clientchange').slideUp(0);
					}
				});
	}