app = angular.module('crm', []);

    app.controller('Clients', function($scope, $http, $httpParamSerializerJQLike) {
        
        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
        
        
        $scope.clients = $scope.newservice = {};
        
        $http.post('/clients/get/').then(function(response){
                    
                    $scope.clients = response.data;

                    $scope.isLoaded = true;
            
                    $('.phone').mask('+7 (999) 999-99-99');
                    
                });
        
        $scope.addClient = function(client) {
            
            wait(1);

            $http.post('/clients/add/', $httpParamSerializerJQLike(client) ).then(function(response){

                        $scope.newclient.$setPristine();
                        $scope.newclient.$setUntouched();
                
                        $scope.client = null;
                
                        client.id = response.data.id;
                
                        $scope.clients.push( client );
                
                        wait(0);
                    
                    });
            
              };
        
        $scope.update = function(index) {
            
            client = $scope.clients[index];
        
            $http.post('/clients/update/', $httpParamSerializerJQLike(client) ).then(function(response) {
                
                if(!response.data.ok){
                    
                    $scope.clients[index] = response.data.old;
                    
                }
                
            });
            
        }
        
        $scope.remove = function(index) {
            
            wait(1);
            
            id = $scope.clients[index].id;
        
            $http.get('/clients/remove/?id=' + id ).then(function(response){
                
                
                $scope.clients.splice(index, 1);
                
                wait(0);
                    
            });
            
            
        }
        
    });
