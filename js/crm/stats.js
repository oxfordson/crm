app = angular.module('crm', []);

app.controller('Stats', ['$scope','$http','$httpParamSerializerJQLike', function($scope, $http, $httpParamSerializerJQLike) {

    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";


    $scope.stats = {};
    
    $scope.int = Number;
    
    //$scope.r = get();
    
    
    $scope.getstats = function(params){
        
            wait(1);

            $http.post('/stats/get/', $httpParamSerializerJQLike(params) ).then(function(response){

                wait(0);

                if(response.data){

                    $scope.stats = response.data;

                }


            });
        
    }
    
    $scope.getstats({});
    
    
    $scope.period = function(period){
        
        
        $scope.stats.p = period;
        
        $scope.apply();
        
    }
    
    $scope.chart = function(chart){
        
        
        $scope.stats.chart = chart;
        
        $scope.apply();
        
    }
    
    $scope.p = function(p){ return (p == $scope.stats.p) }
    
    $scope.c = function(c){ return (c == $scope.stats.chart) }
    
    
    $scope.apply = function(){
        
        
        to  = $scope.stats.to;
        fro = $scope.stats.from;
        
        
        to = to.split("-");
        
        to = new Date( Number( to[2] ), Number( to[1] ) - 1, Number( to[0] ) )
        
        to = Math.round( to.getTime()/1000.0 );
        
        
        
        fro = fro.split("-");
        
        fro = new Date( Number( fro[2] ), Number( fro[1] ) - 1, Number( fro[0] ) )
        
        fro = Math.round( fro.getTime()/1000.0 );
        
        
        $scope.getstats({to: to, from: fro, p: $scope.stats.p, chart: $scope.stats.chart });

        
    }
    
    
        
    $( ".datefrom" ).datepicker({
        
        firstDay: 1,
        nextText: "&#xf105;",
        prevText: "&#xf104;",

        onClose: function( selectedDate, date ) {
        
            $( ".dateto" ).datepicker( "option", "minDate", selectedDate );
            
            $scope.apply();
            
        }

    });
    
    
    $( ".dateto" ).datepicker({
        
        firstDay: 1,
        nextText: "&#xf105;",
        prevText: "&#xf104;",
        
        onClose: function( selectedDate, date ) {
            
            $( ".datefrom" ).datepicker( "option", "maxDate", selectedDate );
            
            $scope.apply();
            
        }

    });
    
    

}]);

app.directive('onlynum', function() {
    return function(scope, element, attrs) {

        var keyCode = [8, 9, 37, 39, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 110];
        element.bind("keydown", function(event) {
            
            if ($.inArray(event.which, keyCode) === -1) {
                scope.$apply(function() {
                    scope.$eval(attrs.onlyNum);
                    event.preventDefault();
                });
                event.preventDefault();
            }

        });
    };
});

app.directive('date', function() {
      
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
 
        ngModel.$validators.match = function(modelValue, viewValue) {
            
                var pattern = new RegExp('^(3[01]|[12][0-9]|0[1-9])-(1[0-2]|0[1-9])-[0-9]{4}$');
            
                check = pattern.test(viewValue);
            
                return check;
        
            }
        }
    }
  });


app.directive('d3chart', function() {
      
    return {
      restrict: 'A',
      scope: {
      
          data: '=',
          tick: '='
      
      },
      link: function(scope, element, attrs) {
          
            svg = d3.select(element[0]).append("svg");
          
            m = { top: 50, right: 30, bottom: 50, left: 5 }

            w = 945 - m.left - m.right;
            h = 220 - m.top - m.bottom;

            chart = svg.attr('width', w)
                       .attr('height', h + m.top + m.bottom)
            
            
            scope.$watch('data', function(newVals, oldVals) {
              return scope.render(newVals);
            }, true);
          
        
            var tooltip = d3.select(".chartTip").style('display', 'none');
            
            
            scope.render = function(data) {
                
                svg.selectAll('*').remove();
                
                if (!data) return;
                
                max = d3.max(data, function(d) { return Number(d.value) } );
                

                y = d3.scale.linear().domain([0, max]).range([0, h]),
                x = d3.scale.linear().domain([0, data.length - 1]).range([0, w - 75])

                g = chart.append("g")
                    .attr('transform', "translate(" + m.left + ", " + (h + m.top) + ")" );

                var line = d3.svg.line()
                    .x(function(d,i) { return x(i); })
                    .y(function(d) { return -1 * y(d.value); })
                    //.interpolate("monotone")
                
                //линия 0
                g.append("line")
                    .attr("class", "axis")
                    .attr("x2", w)
                    .attr("y2", 0)

                //линия верхняя
                g.append("line")
                    .attr("class", "axis")
                    .attr("y1", -1 * h)
                    .attr("x2", w)
                    .attr("y2", -1 * h)
                
                

                //значение верхней линии
                g.append("text")
                      .attr("y", -1 * (h + 15) )
                      .attr("dy", ".71em")
                      .attr("class", "tick")
                      .text( max );

                
                middle = Math.round( max / 2 );
                
                if(middle != max){
                    
                    
                    //средняя линия
                    g.append("line")
                        .attr("class", "axis")
                        .attr("y1", -1 * h / 2)
                        .attr("x2", w)
                        .attr("y2", -1 * h / 2)

                    //значение средней линии
                    g.append("text")
                          .attr("y", -1 * (h + 30) / 2 )
                          .attr("dy", ".71em")
                          .attr("class", "tick")
                          .text( middle );
                    
                }

                //добвляем подписи внизу
                
                g.append('g')
                    .attr('transform', "translate(40, 0)" )
                    .selectAll("g")
                      .data( data )             //.filter( function(d) { return d.text })
                    .enter().append('text')
                      .attr("class", "date")
                      .attr('x', function(d, i) { return x(i) } )
                      .attr('y', 20)
                      .text( function(d) {return d.tick} )


                //рисуем график
                
                g.append("path")
                    .attr("d", line(data))
                    .attr('transform', "translate(40, 0)" )


                //добавляем кружки
                
                if(data.length < 30 ){
                
                    g.append('g')
                        .attr('transform', "translate(40, 0)" )
                        .selectAll("g")
                          .data(data)
                        .enter().append("circle")
                          .attr("class", "points")
                          .attr("r", 5)
                          .attr("cy", function(d) { return -1 * y(d.value) })
                          .attr("cx", function(d, i) { return x(i) })
                    .on("mouseover", function(d, i){ 
                        
                        return tooltip.style('display', 'block')
                        
                                    .style('top', ( h - y(d.value) - 15 ) + 'px')
                                    .style('left', ( x(i) ) + 'px')
                                      .html(d.tooltip)
                    
                    })
                
                }
                
                
                $(document).on('mouseover', function(e){
                    
                    if( $(e.target).closest('.chartTip, .chart').length == 0 ){
                        
                        $('.chartTip').hide(0);
                        
                    }
                    
                });


            }
          
      }
        
    };
      
  });

