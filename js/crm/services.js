app = angular.module('crm', ['ngFileUpload']);

    app.controller('Services', ['$scope', '$http', '$httpParamSerializerJQLike', 'Upload', function($scope, $http, $serialise, Upload) {
        
        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
        
        
        $scope.services = $scope.newservice = [];
        
        $http.post('/services/get/').then(function(response){
                    
                    $scope.services = response.data;
            
                    $scope.isLoaded = true;
                    
                });
        
        $scope.addservice = function(service) {
            
            wait(1);

            $http.post('/services/add/', $serialise(service) ).then(function(response){

                        $scope.newservice.$setPristine();
                        $scope.newservice.$setUntouched();
                
                        $scope.service = null;
                
                        service.id = response.data.id;
                
                        $scope.services.push( service );
                
                        wait(0);
                    
                    });
            
              };
        
        $scope.update = function(index) {
            
            service = $scope.services[index];
        
            $http.post('/services/update/', $serialise(service) ).then(function(response) {
                
                if(!response.data.ok){
                    
                    $scope.services[index] = response.data.old;
                    
                }
                
            });
            
        }
        
        $scope.remove = function(index) {
            
            wait(1);
            
            id = $scope.services[index].id;
        
            $http.get('/services/remove/?id=' + id ).then(function(response){
                
                
                $scope.services.splice(index, 1);
                
                wait(0);
                    
            });
            
        }
        
        $scope.import = function (file) {
        
            if( file ){
                
                modal('.uploadModal');

                delete $scope.error;
                
                if(file.size < 1048576){

                    Upload.upload({

                        url: '/services/import/',
                        data: {csv: file}

                    }).then(function (resp) {

                        if( !resp.data.error ){

                            $scope.services = resp.data.services;
                            
                            modalClose();
                            delete $scope.error;

                        } else {

                            $scope.error = resp.data.error;

                        }

                    }, function (resp) {

                        $scope.error = "Сервер вернул ошибку " + resp.status;

                    }, function (evt) {

                        $scope.progress = parseInt(100.0 * evt.loaded / evt.total) + '%';

                    });
                    
                } else {
                    
                    $scope.error = "Слишком большой файл, максимальный размер файла не должен привышать 1 мегабайт.";
                    
                }
                
            }
        
        };
        
    }]);

app.directive('onlynum', function() {
    return function(scope, element, attrs) {

        var keyCode = [8, 9, 37, 39, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 110];
        element.bind("keydown", function(event) {
            
            if ($.inArray(event.which, keyCode) === -1) {
                scope.$apply(function() {
                    scope.$eval(attrs.onlyNum);
                    event.preventDefault();
                });
                event.preventDefault();
            }

        });
    };
});