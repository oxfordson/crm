app = angular.module('crm', []);

app.controller('Note', ['$scope','$http','$httpParamSerializerJQLike', function($scope, $http, $httpParamSerializerJQLike) {

    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

    $scope.services = {};
    
    $scope.masters = {};
    
    $scope.hours = {};
    
    $scope.note = {};
    
    $scope.int = Number;
    
    $scope.r = get();

    
            
    $http.get('/journal/list/?id=' + $scope.r.id ).then(function(response) {

        $scope.services = response.data;
        
        $scope.note.user = $scope.r.id;
        
    });
    
    
    $scope.chooseService = function(srv){
        
        $scope.note.service = srv.name;
        
        $scope.note.srv_id = srv.id;
        
        $scope.note.price = srv.price;
        
    }
    
    $scope.chooseMaster = function(id){
        
        $scope.note.worker_id = id;
        
    }
    
    $scope.chooseHour = function(unix){
        
        $scope.note.unix = unix;
        
    }
    
    $scope.m = function(id){
        
        return $scope.note.worker_id == id;
        
    }
    
    $scope.h = function(unix){
        
        return $scope.note.unix == unix;
        
    }
    
    $scope.s = function(id){
        
        return $scope.note.srv_id == id;
        
    }
    
    $scope.getHours = function(date){
        
            $http.get('/journal/hours/?master=' + $scope.note.worker_id + '&date=' + date ).then(function(response) {
                
                $('.day').removeClass('active');
                
                $('[date=' + date + ']').addClass('active');
                
                $scope.note.date = date;
                
                $scope.hours = response.data;
                
            });
        
    }
    
    
    $scope.srv = function(){
        
        $('.wnds > div').addClass('hide');
        $('.step-1').removeClass('hide');

        $('.step').removeClass('active');
        $('.service').addClass('active');
        
        $scope.note.worker_id = '';
        
    }
    
    
    $scope.master = function(){
        
        if( $scope.note.service ){
            
            $http.get('/journal/staff/?id=' + $scope.r.id + '&srv=' + $scope.note.srv_id ).then(function(response) {
                
                $('.wnds > div').addClass('hide');
                $('.step-2').removeClass('hide');

                $('.step').removeClass('active');
                $('.master').addClass('active');

                $scope.masters = response.data;

            });
            
        }
        
    }
    
    $scope.datetime = function(){
        
        if( $scope.note.worker_id ){
            
            $('.wnds > div').addClass('hide');
            $('.step-3').removeClass('hide');

            $('.step').removeClass('active');
            $('.date').addClass('active');
            
            
            d = new Date();
            
            cd = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear();
            
            $scope.getHours(cd);
            
            
        }
        
    }
    
    $scope.contacts = function(){
        
        if( $scope.note.unix ){
        
            $('.wnds > div').addClass('hide');
            $('.step-4').removeClass('hide');

            $('.step').removeClass('active');
            $('.contacts').addClass('active');
            
        }
        
    }
    
    $scope.send = function(note) {
    

        $http.post('/journal/save/', $httpParamSerializerJQLike(note) ).then(function(response) {
            
            
            if(response.data.note){
                
                $('.steps, .wnds').addClass('hide');
                
                $('.success').removeClass('hide');
                
                $scope.url = response.data.url;
                
            }
            

        });

    }
    
        
    date = new Date();

    today = new Date();

    today.setDate( today.getDate() + 10 );

    end = new Date();

    end.setDate( end.getDate() + 60 );

    function fill(d){

        months = ['Янв', 'Фев', 'Мар', 'Апр', 'Мая', 'Июня', 'Июля', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'];

        weekdays = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];

        $('#calendar .days').html('');

        for(i=0; i < 10; i++){

            day = d.getDate();
            
            cd = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear();

            month = months[ d.getMonth() ];

            weekday = weekdays[ d.getDay() ];
            
            active = ($scope.note.date == cd) ? 'active' : '';

            $('#calendar .days').append('<div class="day ' + active + '" date="' + cd + '"><div class="weekday">' + weekday + '</div><div class="num">' + day + '</div><div class="month">' + month + '</div></div>');

            d.setDate( day + 1 );
            
            
            
        }

    }

    fill(date);
    
    
    $(document).on('click', '.day', function(e){
        
        d = $(this).attr('date');
        
        $scope.getHours(d);
        
    });


    $('.forth').click(function(){

        if( !$(this).attr('disabled') ){

            fill(date);

            if( date.getTime() >= end.getTime() ){

                $(this).attr('disabled', '');

            }

            $('.back').removeAttr('disabled');

        }

    });

    $('.back').click(function(){

        if( !$(this).attr('disabled') ){

            date.setDate( date.getDate() - 20 );

            fill(date);

            $('.forth').removeAttr('disabled', '');

            if( date.getTime() <= today.getTime() ){

                $('.back').attr('disabled', '');

            }

        }

    });
    

    
    $('.phone').mask('+7 (999) 999-99-99');

}]);