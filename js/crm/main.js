function wait(turn){
    
    if(turn){
        
        $('.loader').fadeIn(0);
        
        $('.blur').css('opacity', '0.2');
    }
    else{
        
        $('.loader').fadeOut(0);
        
        $('.blur').animate({opacity: 1});
    }
    
}

function get(){
    
    params = {};
    
    r = window.location.search;
    
    if(r){
    
        r = r.split('?')[1].split('&');
    
        for(var i in r){
            
            x = r[i].split('=');
            
            params[x[0]] = x[1];
            
        }
        
    }

    return params;
    
}


function modal(element){
    
    $(element).removeClass('hide');
    
    $('.bg').removeClass('hide');
    
    $('.site').addClass('noscroll');
    
    pos = $(element).position();
    
    
    
    $(document).on('scroll', function(){
        
        scroll = $(document).scrollTop();
        
        $(document).css('top', scroll)
        
    });
    
}

function modalClose(){
    
    $('.popup, .bg').addClass('hide');
    
    $('.site').removeClass('noscroll');
    
}

$(function(){

    $('.bg').click( modalClose );
    
    $('[m-dropbox]').click(function(){
        
        $('.dropbox').hide(0);
        
        el = $(this).attr('m-dropbox');
        
        $(el).show(0);
        
    });
    
    $('.openCalendar').click(function(){
        
        
        $('.calendar').toggleClass('hide');
        
    });
    
    
    $(document).on('click touchstart', function(e){
        
        if( $(e.target).closest('[m-dropbox], .dropbox').length == 0){
        
            $('.dropbox').hide(0);
            
        }
        
        if( $(e.target).closest('.openCalendar, .calendar, .ui-icon').length == 0){
        
            $('.calendar').addClass('hide');
            
        }
        
    });

    
    $('[tab]').click(function(){
        
        
        tab = $(this).attr('tab');
        
        $('.tab').addClass('hide');
        
        $('.' + tab).removeClass('hide');
        
        
        $('[tab]').removeClass('active');
        
        $(this).addClass('active');
        
        
    });
    
});






