app = angular.module('crm', []);

    app.controller('Settings', ['$scope', '$http', '$httpParamSerializerJQLike', function($scope, $http, $serialize) {
        
        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
        
        
        $scope.account = {};
        
        $http.post('/settings/getinfo/').then(function(response){
                    
                   $scope.user = response.data;
                    
                });
        
        $scope.send = function(user) {
            
            wait(1);

            $http.post('/settings/saveinfo/', $serialize(user) ).then(function(response){

                        $scope.info.$setPristine();
                        $scope.info.$setUntouched();
                
                        wait(0);
                    
                    });
            
              };
        
        $scope.savemail = function(account) {
            
            wait(1);
        
            $http.get('/settings/savemail/?email=' + account.mail).then(
                function(response) {

                    if (response.data.ok) {
                        
                        $scope.newmail.$setPristine();
                        $scope.newmail.$setUntouched();
                        
                        $scope.user.mail = account.mail;
                        
                        $scope.account = null;
                        
                    }
                    else {
                        
                        $scope.newmail.mail.$setValidity('exists', false);
                        
                    }
                    
                    wait(0);

                });
            
        }
            
         $scope.savephone = function(account) {
             
             wait(1);
        
            $http.post('/settings/savephone/', $serialize({phone: account.phone}) ).then(
                function(response) {

                    if (response.data.ok) {
                        
                        $scope.newphone.$setPristine();
                        $scope.newphone.$setUntouched();
                        
                        $scope.user.phone = account.phone;
                        
                        $scope.account = null;
                        
                    }
                    
                    wait(0);

                });   
            
        }
         
         $scope.savepass = function(account) {
        
            $http.post('/settings/newpass/', $serialize(account) ).then(
                function(response) {
                
                    wait(1);

                    if (response.data.ok) {
                        
                        $scope.newpass.$setPristine();
                        $scope.newpass.$setUntouched();
                        
                        $scope.account = null;
                        
                    }
                    else{
                        
                        $scope.newpass.oldpass.$setValidity('exists', false);
                    }
                    
                    wait(0);

                });   
            
            }
        
    }]);

app.directive('exists', function() {
      return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {

                ngModel.$validators.exists = function(modelValue, viewValue) { return true; }

            }
        }
  });


app.directive('passmatch', function() {
      
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
 
        ngModel.$validators.match = function(modelValue, viewValue) {
            
                return (scope.account.password == modelValue) ? true : false;
            
            }
        }
    }
  });


                
$('.phone').mask('+7 (999) 999-99-99');