app = angular.module('crm', ['ngFileUpload']);

app.controller('Staff', ['$scope', '$http', '$httpParamSerializerJQLike', 'Upload', function($scope, $http, $serialize, Upload) {

    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";


    $scope.staff = {};
        
    $scope.worker = {};
    
    $scope.services = {};
    
    $scope.uploadData = {};
    
    $scope.Int = Number;

    $http.post('/staff/get/').then(function(response){

        $scope.staff = response.data;
        
        $scope.isLoaded = true;

    });

    $scope.addworker = function(worker) {

        wait(1);

        $http.post('/staff/add/', $serialize(worker) ).then(function(response){

                    $scope.nWorker.$setPristine();
                    $scope.nWorker.$setUntouched();

                    $scope.worker = null;

                    $scope.staff.push( response.data.worker );

                    wait(0);

                    modalClose();

                });

          };

    $scope.update = function(index) {

        worker = $scope.staff[index];

        $http.post('/staff/update/', $serialize(worker) ).then(function(response) {

            if(!response.data.ok){

                $scope.staff[index] = response.data.old;

            }

        });

    }

    $scope.remove = function (index) {

        wait(1);

        id = $scope.staff[index].id;

        $http.get('/staff/remove/?id=' + id).then(function (response) {


            $scope.staff.splice(index, 1);

            wait(0);

        });

    }
    
    $scope.Relate = function (id) {

        if ($scope.services.sfw[id]) {

            return 'checked';

        }

    }
 
    $scope.choseService = function(id){
        
        
        one = $('.one#' + id);
        
        
        $http.get('/staff/check/?worker_id=' + worker.id + '&service_id=' + id ).then(function(response){

            $(one).toggleClass('checked');

        });
        
    }
    
    $scope.upload = function (file, worker) {
        
            if( file ){
                
                modal('.uploadModal');
                
                $scope.worker = worker;

                delete $scope.error;
                
                if(file.size < 307200){

                    Upload.upload({

                        url: '/staff/image/',
                        data: {img: file, 'worker_id': worker.id}

                    }).then(function (resp) {

                        worker.image = resp.data.src;

                        if( !resp.data.error ){

                            modalClose();

                            delete $scope.error;

                        } else {

                            $scope.error = resp.data.error;

                        }

                    }, function (resp) {

                        $scope.error = "Сервер вернул ошибку " + resp.status;

                    }, function (evt) {

                        $scope.progress = parseInt(100.0 * evt.loaded / evt.total) + '%';

                    });
                    
                } else {
                    
                    $scope.error = "Слишком большой файл изображения, максимальный размер файла не должен привышать 300 килобайт.";
                    
                }
                
            }
        
        };
        
    $scope.EditServices = function (index) {

        worker = $scope.staff[index];

        wait(1);

        $http.get('/staff/services/?id=' + worker.id).then(function (response) {


            $scope.services = response.data;
            $scope.services.worker = worker;

            modal('.services');

            wait(0);

        });

    }

}]);

app.directive('onlynum', function () {
    return function (scope, element, attrs) {

        var keyCode = [8, 9, 37, 39, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 110];
        element.bind("keydown", function (event) {

            if ($.inArray(event.which, keyCode) === -1) {
                scope.$apply(function () {
                    scope.$eval(attrs.onlyNum);
                    event.preventDefault();
                });
                event.preventDefault();
            }

        });
    };
});