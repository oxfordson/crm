app = angular.module('crm', ['ngDragDrop']);

app.controller('Journal', ['$scope','$http','$httpParamSerializerJQLike', function($scope, $http, $serialise) {

    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

    $scope.services = {};
    $scope.journal = {};
    $scope.note = {};
    
    $scope.focused = false;
    $scope.Int = parseInt;
    
    $scope.r = get();

    
    $scope.getnotes = function(params){
        
        
            wait(1);

            $http.post('/journal/get/', $serialise(params) ).then(function(response){

                wait(0);

                $scope.journal = response.data;

                $scope.isLoaded = true;


            });
        
    }
    
    $scope.getnotes($scope.r);
    
    height = $(window).height();
    
    console.log(height);
    
    if( height > 720){
        
        $('.timetable').css('height', height - 236);
        
    }
    
    $('.timetable').animate({scrollTop: 440});
    
    
    $scope.changeDay = function(unix){

        $scope.getnotes({id: $scope.r.id, unix: unix});
        
    }
    
    

    $scope.pickDay = function(note, day){
        
            $scope.aNote.$setPristine();
            $scope.aNote.$setUntouched();


            $scope.note = note;

            $scope.note.price = (!note.price) ? 0 : note.price;


            $scope.note.index = $scope.journal.days.indexOf(day);

            $scope.note.worker_id = $scope.journal.worker.id;

            modal('.Note');
    
        
        };

    $scope.saveNote = function(note) {
    

        $http.post('/journal/save/', $serialise(note) ).then(function(response) {
            
            
            if(response.data.note){
                
                if(note.index != -1){
                
                    $scope.journal.days[note.index].notes.push(response.data.note);
                    
                }
        
                modalClose();
                
            }

        });

    }
    
    $scope.remove = function(parent, note) {

        $http.get('/journal/remove/?id=' + note.id ).then(function(response) {

            parent.splice(parent.indexOf(note), 1);

        });
        

    }
    
    
    $scope.setStatus = function(note, status) {

        $http.get('/journal/status/?status=' + status + '&id=' + note.id ).then(function(){
            
            note.status = status;
            $('.status').hide(0);
            
            if(status){
            
                $('#note' + note.id).remove();

                num = $('.newnote').length;

                $('.number').html(num);

                if(!num){

                    $('.notifications ul').html('<div class="empty">Нет новых записей</div>');

                    $('.number').remove();

                }
                
            }
            
        });
        

    }
    
    $scope.Status = function(status, cl) {
        
        Class = text = '';
        
        switch ( Number(status) ) {
            case 0:
                
                text = "Новая запись";
    
                break;
                
            case 1:
                
                text = "Подтверждено";
                Class = "confirmed";
                break;
                
            case 2:
                
                text = "Выполнено";
                Class = "done";
                
                break;
        }

        return ( cl ) ? Class : text;
        
    }
    
    
    $scope.autoComplete = function(){
        
            
        $http.get('/services/list/?id=' + $scope.journal.id ).then(function(response) {

            $scope.services = response.data;
            
            $('.autocomplete').removeClass('hide');
            
            $(document).on('click touchstart', function(e){
                
                if( !$(e.target).closest('.autocomplete, input').length ){
                    
                    $('.autocomplete').addClass('hide');
                    
                }
                
                
            });

        });
            
    }
    
    $scope.complete = function(srv){
        
        $scope.note.service = srv.name;
        
        $scope.note.price = srv.price;
        
        $('.autocomplete').addClass('hide');
        
    }
    
        
    $( ".calendar" ).datepicker({
        
        firstDay: 1,
        nextText: "&#xf105;",
        prevText: "&#xf104;",

        onSelect: function( selectedDate, date ) {
            
            d = new Date(date.currentYear, date.currentMonth, date.currentDay);
            
            $scope.r.unix = Math.round( d.getTime()/1000.0 );
            
            $scope.getnotes($scope.r);
            
            $('.calendar').addClass('hide');

            
        }

    });
    
    
    
    $scope.dragStart = function(event, ui){
        
        $scope.dragParams.isDragging = true;
        
        $scope.dragParams.position = $(ui.helper).position();
        
    }
    
    
    $scope.Drop = function(event, ui){
        
        $scope.dragParams.isDragging = false;
        
        
        n = $(ui.helper).scope();
    
        cell = $(event.target).scope();
        
        
        date = cell.day.unixDate;
        
        position = $(ui.helper).position();
        
        
        data = {id: n.note.id, position: position.top, worker_id: n.note.worker_id, date: date};
        
        $http.post('/journal/drop/', $serialise(data) ).then(function(response) {

            
            if(response.data.note){
        
                cell.day.notes.push( response.data.note );
                
                n.$parent.day.notes.splice(n.$index, 1);
                    
            }
            else{
                
                $(ui.helper).animate({'left': 0, 'top': $scope.dragParams.position.top});
                
            }
            
            
        });
        
        
    }
    
    
    $scope.dragParams = {
        
        isDragging: false,
        
        onStart: 'dragStart',
        
    };
    
    $scope.dragOptions = {
    
        revert: 'invalid',
        grid: [ 222, 12 ],
        cursor: "crosshair",
        containment: '.days'
    
    }

    $scope.dropParams = {
        
        onDrop: 'Drop',
        
    };
    
    $scope.resize = function(e, ui){
        
        
        scope = $(e.target).scope();
        
        console.log(scope);
        
        note = scope.note;
        
        height = $(e.target).css('height');
        
        
        data = {id: note.id, height: height, worker_id: note.worker_id};
        
        $http.post('/journal/duration/', $serialise(data) ).then(function(response) {

            
            if(response.data.note){
        
                scope.$parent.day.notes[scope.$index] = response.data.note;
                    
            }
            else{
                
                $(e.target).animate({'height': note.height + 'px'});
                
            }
            
            
        });
        
        
    }
    

    
    $('.phone').mask('+7 (999) 999-99-99');

}]);


app.directive('resizable', function () {

    return {
        restrict: 'A',
        link: function postLink(scope, elem, attrs) {
            
            elem.resizable({
                
                grid: [0, 12],
                minHeight: 24,
                
                stop: function(e, ui){
                    
                    scope.resize(e,ui);
                }
                
            });

        }
    };
  });


app.directive('onlynum', function() {
    return function(scope, element, attrs) {

        var keyCode = [8, 9, 37, 39, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 110];
        element.bind("keydown", function(event) {
            
            if ($.inArray(event.which, keyCode) === -1) {
                scope.$apply(function() {
                    scope.$eval(attrs.onlyNum);
                    event.preventDefault();
                });
                event.preventDefault();
            }

        });
    };
});



app.directive('date', function() {
      
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
 
        ngModel.$validators.match = function(modelValue, viewValue) {
            
                var pattern = new RegExp('^(3[01]|[12][0-9]|0[1-9])-(1[0-2]|0[1-9])-[0-9]{4}$');
            
                check = pattern.test(viewValue);

                return check;
        
            }
        }
    }
  });
