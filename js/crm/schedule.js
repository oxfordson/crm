app = angular.module('crm', []);

app.controller('Schedule', ['$scope','$http','$httpParamSerializerJQLike', function($scope, $http, $httpParamSerializerJQLike) {

    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";


    $scope.schedule = null;
    
    $scope.time = {from: "9:00", to: "18:00"};
    
    $scope.parseInt = parseInt;
    
    $scope.r = get();
    
    
    $scope.getschedule = function(params){
        
            wait(1);

            $http.post('/schedule/get/', $httpParamSerializerJQLike(params) ).then(function(response){

                wait(0);

                if(response.data.worker){

                    $scope.schedule = response.data;

                    $scope.autofill = response.data.autofill;

                }

                $scope.isLoaded = true;


            });
        
    }
    
    $scope.getschedule($scope.r);
    
    $scope.month = function(month){
        
        year = $scope.schedule.year;
        
        if ( month > 12 ) {
            
            month = 1;
            year++;
            
        } else if( month < 1 ) {
            
            month = 12;
            year--;
            
        }

        $scope.getschedule({id: $scope.r.id, month: month, year: year});
        
    }
    
    

    $scope.pickDay = function(index) {
        
                modal('.setTime');
        
                day = $scope.schedule.calendar[index];
                
                if(day.time) {
                    
                    $scope.time.from = day.time.from;
                    $scope.time.to = day.time.to;
                    
                }
        
                $scope.time.date = day.date;
                $scope.time.unix = day.unix;
        
                $scope.time.index = index; 
        
                $scope.time.worker_id = $scope.schedule.worker.id;
        

          };

    $scope.setTime = function(time) {

        $http.post('/schedule/set/', $httpParamSerializerJQLike(time) ).then(function(response) {

            if(response.data.status){
                
                time.id = response.data.id;

                $scope.schedule.calendar[time.index].time = angular.copy(time);
        
                modalClose();

            }

        });

    }
    
    $scope.unsetTime = function(index) {
        
        id = $scope.schedule.calendar[index].time.id;

        $http.get('/schedule/unset/?id=' + id ).then(function(response) {

            delete $scope.schedule.calendar[index].time;

        });
        

    }
    
    $scope.fill = function(autofill) {
        
        autofill.id = $scope.schedule.worker.id;
        
        $http.post('/schedule/fill/', $httpParamSerializerJQLike(autofill) ).then(function(response) {
            
            
            $scope.r.id = $scope.schedule.worker.id;
            $scope.r.month = $scope.schedule.month;
            
            $scope.getschedule($scope.r);
            
            modalClose();

        });
        

    }
        
    $( ".datefrom" ).datepicker({
        
        firstDay: 1,
        nextText: "&#xf105;",
        prevText: "&#xf104;",

        onClose: function( selectedDate, date ) {
            
            d = new Date(date.currentYear, date.currentMonth, date.currentDay);
            
            d.setMonth(d.getMonth() + 3);
            
            $( ".dateto" ).datepicker( "option", "minDate", selectedDate );
            $( ".dateto" ).datepicker( "option", "maxDate", d );
            
        }

    });
    
    
    $( ".dateto" ).datepicker({
        
        firstDay: 1,
        nextText: "&#xf105;",
        prevText: "&#xf104;",
        maxDate: "+3m",
        
          onClose: function( selectedDate, date ) {
              
            d = new Date(date.currentYear, date.currentMonth, date.currentDay);
            
            d.setMonth(d.getMonth() - 3);

            $( ".datefrom" ).datepicker( "option", "maxDate", selectedDate );
              
            $( ".datefrom" ).datepicker( "option", "minDate", d );

          }

    });
    
    

}]);

app.directive('onlynum', function() {
    return function(scope, element, attrs) {

        var keyCode = [8, 9, 37, 39, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 110];
        element.bind("keydown", function(event) {
            
            if ($.inArray(event.which, keyCode) === -1) {
                scope.$apply(function() {
                    scope.$eval(attrs.onlyNum);
                    event.preventDefault();
                });
                event.preventDefault();
            }

        });
    };
});

app.directive('mchoose', function() {
    return {
        
        restrict: 'A',
        scope: {
        
            val: '=mchoose'
        
        },
        
        link: function(scope, element, attrs) {
        
            element.children('li').on("click", function(event) {

                scope.val = $(this).html();

                scope.$apply();
                
                $('.dropbox').hide(0);

            });
        }
        
    }
});

app.directive('date', function() {
      
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
 
        ngModel.$validators.match = function(modelValue, viewValue) {
            
                var pattern = new RegExp('^(3[01]|[12][0-9]|0[1-9])-(1[0-2]|0[1-9])-[0-9]{4}$');
            
                check = pattern.test(viewValue);
            
                //ngMode.$setValidity('date', isValid);
            
                return check;
        
            }
        }
    }
  });

