app = angular.module('crm', []);

    app.controller('RegController', ['$scope','$http','$httpParamSerializerJQLike', function($scope, $http, $httpParamSerializerJQLike) {
        
        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
        
        $scope.user = {};
        
        $scope.send = function(user) {
            
                $http.post('/user/registrate/', $httpParamSerializerJQLike(user) ).then(function(response){
                    
                   if(response.data.ok) window.location = '/settings/';
                    
                });

            
              };
        
    }]);

app.directive('usermail', function($q, $http) {
    
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
 
        ngModel.$asyncValidators.usermail = function(modelValue, viewValue) {
            
            return $http.get('/user/email/?email=' + modelValue).then(
                function(response) {

                    if (response.data.user ) {
                        return $q.reject('exists');
                    }
                    

                    return true;
                });
            }
        }
    }
  });

app.directive('passmatch', function() {
      
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
 
        ngModel.$validators.pass = function(modelValue, viewValue) {
            
                return (scope.user.password == modelValue) ? true : false;
            
            }
        }
    }
  
  });


                
$('.phone').mask('+7 (999) 999-99-99');